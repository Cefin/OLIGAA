# Suivi des évolutions

## 1.4.x

### Nouvelles fonctionnalités

- Fil d'information des clubs

### Corrections

- Mise à jour du *copyright*
- [1.4.1] Personnalisation des titre de menu possible (#100)
- [1.4.2] Expéditeur de mail par défaut en variable d'environnement
- [1.4.3] Ajout pot de miel pour formulaire de contact (#108)

## 1.3.x

### Nouvelles fonctionnalités

- Évolution de la Plateforme Sport et Handicap Psychique :
  - gestion des saisons
  - renouvellement des adhésions

### Corrections

- [1.3.1] Saisons absentes dans le menu déroulant (#97)

## 1.2.x

### Nouvelles fonctionnalités

- Mise en place de la Plateforme Sport et Handicap Psychique :
  - gestion des séances par les clubs partenaires
  - création des adhésions par les clubs partenaires
  - suivi des adhésions et de la facturation par les administrateurs
- Migration de Google Maps vers Openstreetmap (#12)

### Corrections

- [1.2] Liens cassés dans le PDF du calendrier (#91)
- [1.2] Débordement des grandes images (#65)
- [1.2] Mauvaise redirection après édition d'un membre (#69)
- [1.2] Listing des institutions par ordre alphabétiue (#87)

## 1.1.x

### Nouvelles fonctionnalités

- Possibilité d'associer plusieurs images à chaque item d'une lettre d'information et de les afficher en plein écran (#1 et #2)
- Affichage d'une mini carte sur la page d'un événement (#8)
  - Note : pour géolocaliser les événements déjà enregistrés, utiliser la commande `bundle exec rake geocode:all CLASS=Event SLEEP=0.25 BATCH=100`
- Possibilité de dupliquer une information depuis le listing (#9)

### Corrections

- [1.1] Problème à l'édition des numéros de téléphone d'un contact (#14)
- [1.1.1] Problème à la génération PDF des lettres d'information (#2)
- [1.1.2] Problème d'affichage des cartes Google Maps (#21)
- [1.1.3] Correction du *copyright* (#18)
- [1.1.3] Problème à l'impression d'un calendrier (#5)
- [1.1.4] Liens cassés dans les PDF des calendriers (#22)
- [1.1.5] Problème d'affichage du lien "Éditer" sur les autres fiches-membres (#24)
- [1.1.5] Intégration des polices d'écriture à l'application (#13)
- [1.1.5] Mise à jour de Rails à la version 3.2.22.5 (#25)
- [1.1.6] Compatibilité complète avec SSL/TLS (#26)
- [1.1.7] Possibilité de renommer les menus *fixes* (#27)
- [1.1.8] Passage à Ruby 2.3.4
- [1.1.9] N'afficher le menu *Où pratiquer* que si nécessaire (#54)
- [1.1.10] Courriel avec uniquement des destinataires autres (#62)
- [1.1.11] Id client Google Oauth 2 en variable d'environnement

## 1.0 - Version initiale
