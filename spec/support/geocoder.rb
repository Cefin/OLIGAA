#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
Geocoder.configure(:lookup => :test)

Geocoder::Lookup::Test.add_stub(
  "Bordeaux, France", [
    {
      'latitude'     => 44.837789,
      'longitude'    => -0.57918,
      'address'      => 'Mairie',
      'state'        => 'Bordeaux',
      'state_code'   => '33000',
      'country'      => 'France',
      'country_code' => 'FR'
    }
  ]
)

Geocoder::Lookup::Test.add_stub(
  "Mairie, Arcachon, France", [
    {
      'latitude'     => 44.652297,
      'longitude'    => -1.1785016,
      'address'      => 'Mairie',
      'state'        => 'Arcachon',
      'state_code'   => '33120',
      'country'      => 'France',
      'country_code' => 'FR'
    }
  ]
)

Geocoder::Lookup::Test.add_stub(
  "Mairie, Langon, France", [
    {
      'latitude'     => 47.700983,
      'longitude'    => -1.9346748,
      'address'      => 'Mairie',
      'state'        => 'Langon',
      'state_code'   => '33210',
      'country'      => 'France',
      'country_code' => 'FR'
    }
  ]
)

Geocoder::Lookup::Test.set_default_stub(
  [
    {
      'latitude'     => 40.7143528,
      'longitude'    => -74.0059731,
      'address'      => 'New York, NY, USA',
      'state'        => 'New York',
      'state_code'   => 'NY',
      'country'      => 'United States',
      'country_code' => 'US'
    }
  ]
)
