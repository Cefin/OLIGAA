#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module FeatureMacros
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
  end

  def default_password
    'pass'
  end

  def login_as_member
    @member = create :member, name: "Normal Member"
    login_as(@member.login, default_password)
  end

  def login_as_admin
    @admin = create :member, :admin, name: "Admin"
    login_as(@admin.login, default_password)
  end

  def login_as_collaborating_club
    mg = create :member_group
    @settings.platform_collaborating_clubs << mg
    @member = create :member, name: "Collaborating club", member_groups: [mg]
    login_as(@member.login, default_password)
  end

  def login_as_collaborating_medicosocial_org
    mg = create :member_group
    @settings.platform_collaborating_medicosocial_orgs << mg
    @member = create :member, name: "Collaborating medicosocial org", member_groups: [mg]
    login_as(@member.login, default_password)
  end

  def login_as(login, pass)
    visit login_path
    within "form#sign_in" do
      fill_in :login, with: login
      fill_in :password, with: pass
      click_button "S'identifier"
    end
  end

  def logout
    visit logout_path
  end

  def click_edit_link
    click_link "Éditer"
  end

  def click_delete_link
    click_link "Supprimer"
  end

  def click_move_up_link
    click_link "Monter"
  end

  def click_move_down_link
    click_link "Descendre"
  end

  def click_create_button
    click_button "Créer"
  end

  def click_update_button
    click_button "Mettre à jour"
  end

  def set_laptop_size
    resize_window_to(800, 600)
  end

  private

  def resize_window_to(width, height)
    page.current_window.resize_to(width, height)
  end
end
