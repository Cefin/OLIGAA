#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage club novels" do

  before(:each) { create :menu, title: "Info des clubs Sport Adapté Girondins", uid: "club_novels" }

  context "as an admin" do

    before { login_as_admin }

    scenario "I can create one" do
      new_club_novel = build :club_novel

      visit club_novels_path
      within "section#content" do
        click_link "Nouvelle info club"
      end

      expect(current_path).to eq(new_club_novel_path)
      within "form" do
        fill_in "Intitulé", with: new_club_novel.title
        fill_in "Contenu", with: new_club_novel.content
        click_create_button
      end

      expect(current_path).to eq club_novels_path
      within "section#club_novels"  do
        expect(page).to have_selector("h2", text: new_club_novel.title)
        expect(page).to have_content new_club_novel.content
      end
    end

    context "regarding edition" do

      let!(:club_novel) { create :club_novel }

      scenario "I can change its info" do
        new_title = "Nouveau titre"

        visit club_novels_path
        within "section#club_novels" do
          click_edit_link
        end

        expect(current_path).to eq(edit_club_novel_path(club_novel))
        within "form" do
          fill_in "Intitulé", with: new_title
          click_update_button
        end

        expect(current_path).to eq club_novels_path
        within "section#club_novels"  do
          expect(page).to have_selector("h2", text: new_title)
        end
      end

      scenario "I can [un-]publish one" do
        visit edit_club_novel_path(club_novel)
        within "form" do
          check "Publiée"
          click_update_button
        end

        within "section#club_novels" do
          expect(page).not_to have_selector("ul#unpublished")
          within "ul#published" do
            expect(page).to have_selector("h2", text: club_novel.title)
          end
        end

        visit edit_club_novel_path(club_novel)
        within "form" do
          uncheck "Publiée"
          click_update_button
        end

        within "section#club_novels" do
          expect(page).not_to have_selector("ul#published")
          within "ul#unpublished" do
            expect(page).to have_selector("h2", text: club_novel.title)
          end
        end
      end

      context "regarding images", :js do
        scenario "I can attach at least 2 images" do
          image1 = build :image, title: "Image 1"
          image2 = build :image, title: "Image 2"
          visit edit_club_novel_path club_novel
          within "form" do
            click_link "Images"
            click_link "Ajouter une image"
            first_image_form = all("div.edit_image")[0]
            first_image_form.fill_in("Titre", with: image1.title)
            first_image_form.attach_file("Fichier", sample_image_path)
            click_link "Ajouter une image"
            second_image_form = all("div.edit_image")[1]
            second_image_form.fill_in("Titre", with: image2.title)
            second_image_form.attach_file("Fichier", sample_image_path)
            click_update_button
          end

          expect(current_path).to eq club_novels_path
          within "section#content li", text: club_novel.title do
            within "div.aside" do
              within "ul.gallery" do
                expect(first('img')[:alt]).to eq(image1.title)
                expect(page).to have_css('img', visible: false, count: 2)
              end
              within "span.images-number" do
                expect(page).to have_content "2"
              end
            end
          end
        end

        scenario "I can remove an image" do
          image = create :image, imageable: club_novel
          visit edit_club_novel_path(club_novel)
          within "form" do
            click_link "Images"
            click_link "Supprimer l'image"
            click_update_button
          end

          expect(current_path).to eq club_novels_path
          within "section#content li", text: club_novel.title do
            expect(page).not_to have_selector "img"
            expect(page).not_to have_selector "span.images-number"
          end
        end
      end

      context "regarding attached_files" do
        scenario "I can attach a file", :js do
          attached_file = build :attached_file
          visit edit_club_novel_path club_novel
          within "form" do
            click_link "Pièces jointes"
            click_link "Ajouter une pièce jointe"
            fill_in "Nom", with: attached_file.name
            attach_file "Fichier", sample_text_path
            click_update_button
          end

          expect(current_path).to eq club_novels_path
          within "section#content li", text: club_novel.title do
            expect(page).to have_selector("a", text: attached_file.name)
          end
        end

        scenario "I can remove a file", :js do
          attached_file = create :attached_file, attachable: club_novel
          visit edit_club_novel_path club_novel
          within "form" do
            click_link "Pièces jointes"
            click_link "Supprimer la pièce jointe"
            click_update_button
          end

          expect(current_path).to eq club_novels_path
          within "section#content li", text: club_novel.title do
            expect(page).not_to have_selector("a", text: attached_file.name)
          end
        end
      end

      scenario "I can dis-associate activities to any" do
        act1 = create :activity
        act2 = create :activity
        club_novel.activities << act1
        visit edit_club_novel_path club_novel
        within "form" do
          click_link "Activités"
          within "span.checkbox", text: act1.title do
            expect(find("input[type=checkbox]")).to be_checked
          end
          within "span.checkbox", text: act2.title do
            expect(find("input[type=checkbox]")).not_to be_checked
          end
          check act2.title
          uncheck act1.title
          click_update_button
        end

        expect(current_path).to eq club_novels_path
        within "section#content" do
          expect(page).not_to have_content act1.title
          expect(page).to have_content act2.title
        end
      end
    end

    scenario "I can delete one" do
      club_novel = create :club_novel

      visit club_novels_path
      expect(page).to have_content(club_novel.title)

      within "section#club_novels" do
        click_delete_link
      end

      expect(current_path).to eq club_novels_path
      expect(page).not_to have_content(club_novel.title)
    end

    scenario "I can see all club_novels" do
      published_club_novel = create :club_novel, :published
      unpublished_club_novel = create :club_novel

      visit club_novels_path
      within "section#club_novels" do
        within "ul#unpublished" do
          expect(all("li").length).to eq 1
          expect(page).to have_selector("h2", text: unpublished_club_novel.title)
        end

        within "ul#published" do
          expect(all("li").length).to eq 1
          expect(page).to have_selector("h2", text: published_club_novel.title)
        end
      end
    end
  end

  context "as a guest" do
    scenario "I have access to the 4 latest published ones" do
      3.times { create :club_novel }
      6.times { create :club_novel, :published }

      visit root_path
      within "nav#main-nav" do
        click_link "Info des clubs Sport Adapté Girondins"
      end

      expect(current_path).to eq club_novels_path
      within "section#content" do
        expect(page).to have_content "Info des clubs Sport Adapté Girondins"
        expect(page).not_to have_selector("ul#unpublished")
        within "ul#published" do
          expect(all("li").count).to eq 4
        end

        click_link "Suivants"
        expect(page).not_to have_selector("ul#unpublished")
        expect(page).to have_selector("ul#published")
      end
    end
  end
end
