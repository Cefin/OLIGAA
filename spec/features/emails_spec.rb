#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Emails" do
  context "as an admin" do
    before { login_as_admin }

    context "regarding creation" do
      before do
        @member = create :member
        @contact = @member.contacts.first
      end

      def fill_in_common_things
        within "#sender" do
          select @admin.contacts.first.email
        end
        find("h3", text: "Destinataires").click
        within "#recipients" do
          click_link "Membres sans groupe"
          click_link @member.name
          check @contact.email
          find('.switch-button-label', text: "Autres destinataires").click
          fill_in :email_other_recipients, with: "moi@example.com"
        end
      end

      scenario "I can create one", :js do
        visit root_path
        within "nav#admin_links" do
          click_link "Emails"
        end

        expect(current_path).to eq(emails_path)
        within "section#content" do
          click_link "Nouvel email"
        end

        expect(current_path).to eq(new_email_path)
        within "form" do
          fill_in_common_things
          find("h3", text: "Information").click
          within "#information" do
            fill_in "Sujet", with: "Nouvel Email"
            fill_in "Contenu", with: "Contenu du nouvel email."
          end
          click_create_button
        end

        expect(current_path).to eq(email_path(Email.first))
        within "section#content" do
          expect(page).to have_content "Email créé avec succès."
          expect(page).to have_selector("h1", text: "Email - Nouvel Email")
          expect(page).to have_content "Contenu du nouvel email."
          expect(page).to have_content @contact.email
          expect(page).to have_content "untel@example.com"
        end
      end

      scenario "I can create one from an existing novel", :js do
        novel = create :novel
        visit novel_path novel
        click_link "Envoyer par mail"

        expect(current_path).to eq(new_email_path)
        within "form" do
          fill_in_common_things
          click_create_button
        end

        expect(current_path).to eq(email_path(Email.first))
        within "section#content" do
          expect(page).to have_content "Email créé avec succès."
          expect(page).to have_selector("h1", text: "Email - #{novel.title}")
          expect(page).to have_content novel.content
          expect(page).to have_content @contact.email
          expect(page).to have_content "untel@example.com"
        end
      end

    end

    scenario "I can delete one", :js do
      email = create :email, novel: (create :novel)
      novel = email.novel
      visit emails_path
      within "section#content" do
        click_link "Non envoyés"
        within "#email_list ul > li", text: novel.title do
          click_delete_link
        end
      end

      expect(current_path).to eq(emails_path)
      within "section#content" do
        expect(page).to have_content "Email supprimé avec succès."
        click_link "Non envoyés"
        expect(page).not_to have_selector("li", text: novel.title)
      end
    end

    context "regarding edition" do
      before do
        @contact = create :contact, email: "recipient@example.com"
        @member = create :member, contacts: [@contact]
        @email = create :email, novel: (create :novel), sender: @admin.contacts.first.email, recipients: @contact.email
      end

      scenario "I can change the sender", :js do
        new_contact_email = "new@example.com"
        contact = create :contact, email: new_contact_email
        @admin.contacts << contact
        novel = @email.novel
        visit emails_path
        within "section#content" do
          click_link "Non envoyés"
          within "#email_list ul > li", text: novel.title do
            click_edit_link
          end
        end

        expect(current_path).to eq(edit_email_path(@email))
        within "form" do
          select new_contact_email
          click_update_button
        end

        expect(current_path).to eq(email_path(@email.reload))
        within "section#content" do
          expect(page).to have_content "Email modifié avec succès."
          expect(page).to have_content new_contact_email
        end
      end

      scenario "I can change the recipients", :js do
        new_member = create :member
        new_recipient = "recipient-new@example.com"
        new_member.contacts.first.update_attribute(:email, new_recipient)
        old_recipient = @contact.email
        visit edit_email_path @email
        within "form" do
          find("h3", text: "Destinataires").click
          uncheck old_recipient
          click_link new_member.name
          check new_member.contacts.first.full_info
          click_update_button
        end

        expect(current_path).to eq(email_path(@email.reload))
        within "section#content" do
          expect(page).to have_content "Email modifié avec succès."
          expect(page).to have_content new_recipient
          expect(page).not_to have_content old_recipient
        end
      end

      scenario "I can change the associated novel", :js do
        old_subject = "Ancien sujet du mail"
        new_subject = "Nouveau sujet du mail"
        @email.novel.update_attribute(:title, old_subject)
        visit edit_email_path @email
        within "form" do
          find("h3", text: "Information").click
          within "#information" do
            fill_in "Sujet", with: new_subject
          end
          click_update_button
        end

        expect(current_path).to eq(email_path(@email.reload))
        within "section#content" do
          expect(page).to have_content "Email modifié avec succès."
          expect(page).to have_selector("h1", text: "Email - #{new_subject}")
        end

        visit novels_path
        click_link "Informations sans type"
        within "ul#untyped" do
          expect(page).to have_selector("li", text: new_subject)
          expect(page).not_to have_selector("li", text: old_subject)
        end
      end
    end

    scenario "I can send one" do
      new_mail = create :email, novel: (create :novel)
      visit email_path new_mail

      expect do
        click_link "Envoyer"
      end.to change(Delayed::Job, :count).by 3

      expect(current_path).to eq(email_path(new_mail))
      within "section#content" do
        expect(page).to have_content "L'envoi de l'e-mail a bien été planifié. Il partira dans quelques secondes."
      end
    end
  end
end
