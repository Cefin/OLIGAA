#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Novel Types" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      nt = build :novel_type
      visit root_path
      within "nav#admin_links" do
        click_link "Informations"
      end

      expect(current_path).to eq(novels_path)
      within "section#content" do
        click_link "Nouveau type d'information"
      end

      expect(current_path).to eq(new_novel_type_path)
      within "form" do
        fill_in "Intitulé", with: nt.title
        fill_in "Description", with: nt.description
        click_create_button
      end

      expect(current_path).to eq(novel_type_path(NovelType.first))
      within "section#content" do
        expect(page).to have_content "Type d'information créé avec succès."
        expect(page).to have_selector("h1", text: nt.title)
        expect(page).to have_content nt.description
      end
    end

    scenario "I can delete one", :js do
      nt = create :novel_type
      visit novels_path
      within "#novel_list" do
        within "li", text: nt.title do
          click_link nt.title
          within "p.AdminCard-headingActions" do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(novels_path)
      within "section#content" do
        expect(page).to have_content "Type d'information supprimé avec succès."
        expect(page).not_to have_content nt.title
      end
    end

    scenario "I can edit one", :js do
      nt = create :novel_type
      new_title = "Nouveau Titre"
      visit novels_path
      within "#novel_list" do
        within "li", text: nt.title do
          click_link nt.title
          within "p.AdminCard-headingActions" do
            click_edit_link
          end
        end
      end

      expect(current_path).to eq(edit_novel_type_path(nt))
      within "form" do
        fill_in "Intitulé", with: new_title
        click_update_button
      end

      expect(current_path).to eq(novel_type_path(nt.reload))
      within "section#content" do
        expect(page).to have_content "Type d'information mis à jour avec succès."
        expect(page).to have_selector("h1", text: new_title)
      end
    end
  end
end