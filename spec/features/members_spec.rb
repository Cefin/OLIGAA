#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage members" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create a member", :js do
      member = build :member
      contact = member.contacts.first
      visit root_path
      within "nav#admin_links" do
        click_link "Membres"
      end
      within "section#content" do
        click_link "Nouveau membre"
      end
      within "form" do
        within "#general" do
          fill_in "Dénomination", with: member.name
          fill_in "Identifiant", with: member.login
        end
        click_link "Contacts"
        within "#contacts" do
          fill_in "Nom", with: contact.last_name
          fill_in "Prénom", with: contact.first_name
          fill_in "Adresse email", with: contact.email
        end
        click_create_button
      end

      expect(current_path).to eq(members_path)
      within "section#content" do
        click_link "Membres sans groupe"
        expect(page).to have_content "Le compte a maintenant été créé."
        expect(page).to have_selector("li", text: member.name)
      end
    end

    scenario "I can delete a member", :js do
      member = create :member
      visit root_path
      within "nav#admin_links" do
        click_link "Membres"
      end
      click_link "Membres sans groupe"
      within "ul#ungrouped li", text: member.name do
        click_delete_link
      end

      expect(current_path).to eq(members_path)
      within "section#content" do
        expect(page).to have_content "Compte supprimé avec succès."
        click_link "Membres sans groupe"
        expect(page).not_to have_selector("li", text: member.name)
      end
    end

    context "regarding edition" do
      before { @member = create :member }

      scenario "I can modify general information", :js do
        new_name = "Nouvelle dénomination"
        visit root_path
        within "nav#admin_links" do
          click_link "Membres"
        end
        click_link "Membres sans groupe"
        within "ul#ungrouped li", text: @member.name do
          click_edit_link
        end

        expect(current_path).to eq(edit_member_path(@member))
        within "form" do
          fill_in "Dénomination", with: new_name
          click_update_button
        end

        expect(current_path).to eq(members_path)
        within "section#content" do
          expect(page).to have_content "Compte mis à jour avec succès."
          click_link "Membres sans groupe"
          expect(page).to have_selector("li", text: new_name)
          expect(page).not_to have_selector("li", text: @member.name)
        end
      end

      context "regarding contacts", :js do

        scenario "I can add one" do
          new_contact = build :contact, last_name: "Dupont", first_name: "Jean", email: "jean.dupont@example.com", member: nil
          visit edit_member_path @member
          within "form" do
            click_link "Contacts"
            within "#contacts" do
              click_link "Ajouter un contact"
              within all("li.edit_contact")[1] do
                fill_in "Nom", with: new_contact.last_name
                fill_in "Prénom", with: new_contact.first_name
                fill_in "Adresse email", with: new_contact.email
              end
            end
            click_update_button
          end

          expect(current_path).to eq(member_path(@member))
          within "section#content" do
            expect(page).to have_content "Compte mis à jour avec succès."
          end

          visit members_path
          click_link "Membres sans groupe"
          within "ul#ungrouped" do
            click_link "#{@member.name} - #{@member.login}"
          end

          expect(current_path).to eq(member_path(@member))
          within "#private-stuff" do
            expect(page).to have_content new_contact.full_info
          end
        end

        scenario "I can remove one" do
          create :contact, member: @member
          visit edit_member_path @member
          within "form" do
            click_link "Contacts"
            expect(all("li.edit_contact").length).to eq(2)
            within all("li.edit_contact")[1] do
              click_link "Supprimer le contact"
            end
            click_update_button
          end

          expect(current_path).to eq(member_path(@member))
          within "section#content" do
            expect(page).to have_content "Compte mis à jour avec succès."
          end

          visit members_path
          click_link "Membres sans groupe"
          within "ul#ungrouped" do
            click_link "#{@member.name} - #{@member.login}"
          end

          expect(current_path).to eq(member_path(@member))
          within "#private-stuff" do
            within "ul#contacts" do
              expect(all("li").length).to eq(1)
            end
          end
        end

        context "regarding edition" do
          scenario "I can modify one" do
            new_contact = build :contact, last_name: "Dupont", first_name: "Jean", email: "jean.dupont@example.com", member: nil
            visit edit_member_path @member
            within "form" do
              click_link "Contacts"
              within first("li.edit_contact") do
                fill_in "Nom", with: new_contact.last_name
                fill_in "Prénom", with: new_contact.first_name
                fill_in "Adresse email", with: new_contact.email
              end
              click_update_button
            end

            expect(current_path).to eq(member_path(@member))
            within "section#content" do
              expect(page).to have_content "Compte mis à jour avec succès."
            end

            visit members_path
            click_link "Membres sans groupe"
            within "ul#ungrouped" do
              click_link "#{@member.name} - #{@member.login}"
            end

            expect(current_path).to eq(member_path(@member))
            within "#private-stuff" do
              within "ul#contacts" do
                expect(page).to have_content new_contact.full_info
              end
            end
          end
        end

        context "regarding phone numbers" do
          scenario "I can add one" do
            phone = create :phone_number
            visit edit_member_path @member
            within "form" do
              click_link "Contacts"
              within first("li.edit_contact") do
                click_link "Plus d'info"
                click_link "Ajouter un numéro"
                fill_in "Numéro", with: phone.number
                fill_in "Type", with: phone.kind
              end
              click_update_button
            end

            expect(current_path).to eq(member_path(@member))
            expect(page).to have_content "Compte mis à jour avec succès."

            visit member_path @member
            within "aside#private-stuff" do
              expect(page).to have_content "#{phone.kind} : #{phone.number}"
            end
          end

          scenario "I can remove one" do
            phone = create :phone_number
            @member.contacts.first.phone_numbers << phone
            new_phone = "0605040302"
            new_kind = "mobile"

            visit edit_member_path @member
            within "form" do
              click_link "Contacts"
              within first("li.edit_contact") do
                click_link "Plus d'info"
                fill_in "Numéro", with: new_phone
                fill_in "Type", with: new_kind
              end
              click_update_button
            end

            expect(current_path).to eq(member_path(@member))
            expect(page).to have_content "Compte mis à jour avec succès."

            visit member_path @member
            within "aside#private-stuff" do
              expect(page).not_to have_content "#{phone.kind} : #{phone.number}"
              expect(page).to have_content "#{new_kind} : #{new_phone}"
            end
          end

          scenario "I can edit one" do
            phone = create :phone_number
            @member.contacts.first.phone_numbers << phone

            visit member_path @member
            within "aside#private-stuff" do
              expect(page).to have_content "#{phone.kind} : #{phone.number}"
            end

            visit edit_member_path @member
            within "form" do
              click_link "Contacts"
              within first("li.edit_contact") do
                click_link "Plus d'info"
                click_link "Supprimer le numéro"
              end
              click_update_button
            end

            expect(current_path).to eq(member_path(@member))
            expect(page).to have_content "Compte mis à jour avec succès."

            visit member_path @member
            within "aside#private-stuff" do
              expect(page).not_to have_content "#{phone.kind} : #{phone.number}"
            end
          end
        end
      end

      scenario "I can associate activities" do
        act1 = create :activity
        @member.activities << act1
        act2 = create :activity
        visit edit_member_path @member
        within "form" do
          click_link "Activités"
          within "div.member_activities" do
            uncheck act1.title
            check act2.title
          end
          click_update_button
        end

        visit member_path @member
        within "div#public-stuff" do
          expect(page).not_to have_content act1.title
          expect(page).to have_content act2.title
        end
      end

      scenario "I cannot change the password of another member" do
        visit edit_member_path @member
        within "form" do
          expect(page).not_to have_selector("div#change-password")
        end
      end

      scenario "I can reset the password", :js do
        contact = create :contact, email: "jean.dupont@example.com", member: @member
        visit members_path
        within "section#content" do
          click_link "Membres sans groupe"
          within "ul#ungrouped li", text: @member.name do
            click_link "RAZ MDP"
          end
        end
        expect(current_path).to eq(members_path)
        within "section#content" do
          expect(page).to have_content "Mot de passe du membre remis à zéro avec succès. Email envoyé avec succès à : #{@member.contacts.first.email}, jean.dupont@example.com."
        end
      end

      scenario "I can associate to member groups", :js do
        mg = create :member_group
        visit edit_member_path @member
        within "form" do
          check mg.title
          click_update_button
        end

        expect(current_path).to eq(member_path(@member))
        within "section#content" do
          expect(page).to have_content "Compte mis à jour avec succès."
        end

        visit members_path
        click_link mg.title

        expect(page).to have_selector("li", text: @member.name)
      end

      scenario "I can set its registry date" do
        visit edit_member_path @member
        registry_date = "06/12/2013"
        within "form" do
          fill_in "Date d'enregistrement", with: registry_date
          click_update_button
        end

        expect(current_path).to eq(member_path(@member))
        within "section#content" do
          expect(page).to have_content "Compte mis à jour avec succès."
          expect(page).to have_content registry_date
        end
      end
    end

    scenario "I can search within contacts" do
      me = create :contact, last_name: "Dupont", first_name: "Jean", email: "jean.dupont@example.com"
      my_member = create :member, contacts: [me]
      other =  create :contact, last_name: "Other", first_name: "Person", email: "other.person@example.com"
      other_member = create :member, contacts: [other]
      visit members_path
      within "#search_contacts_form" do
        fill_in :contacts_search, with: "jean"
        click_button "Chercher dans les contacts"
      end

      expect(current_path).to eq(search_contacts_members_path)
      within "section#content" do
        expect(page).to have_link my_member.name
        expect(page).to have_content me.full_info
        expect(page).not_to have_content other_member.name
      end
    end

    context "regarding map location status" do
      scenario "indicates not located for an unlocated member" do
        member = create :member, address: ""
        visit edit_member_path member
        within "#information" do
          expect(page).to have_selector("span.error_message", text: "Non trouvé sur la carte")
        end
      end

      scenario "indicates located for a located members" do
        member = create :member, latitude: "+0", longitude: "+0"
        visit edit_member_path member
        within "#information" do
          expect(page).to have_selector("span.notice_message", text: "Affiché sur la carte")
        end
      end
    end

    context "regarding the private map" do
      scenario "I can display it" do
        visit members_path
        click_link "Affichage carte"

        expect(current_path).to eq(map_members_path)
        within "section#content" do
          expect(page).to have_content("Aucun élément n'a été trouvé")
        end
      end

      scenario "I can filter by member groups" do
        mg1 = create :member_group
        mg2 = create :member_group
        m1 = create :member, member_groups: [mg1]
        m2 = create :member, member_groups: [mg2]
        visit map_members_path

        expect(page).not_to have_selector("div#displayed_members")

        within "form#members_filter" do
          check mg1.title
          click_button "Filtrer"
        end

        expect(current_path).to eq(map_members_path)
        within "div#displayed_members" do
          expect(page).to have_selector("li", text: m1.name)
          expect(page).not_to have_selector("li", text: m2.name)
        end

        within "form#members_filter" do
          check mg2.title
          click_button "Filtrer"
        end

        expect(current_path).to eq(map_members_path)
        within "div#displayed_members" do
          expect(page).to have_selector("li", text: m1.name)
          expect(page).to have_selector("li", text: m2.name)
        end
      end

      scenario "I can filter by registry date" do
        m2012 = create :member, registry_date: "01/07/2012"
        m2013 = create :member, registry_date: "01/07/2013"
        visit map_members_path


        expect(page).not_to have_selector("div#displayed_members")

        within "form#members_filter" do
          check "2012"
          click_button "Filtrer"
        end

        expect(current_path).to eq(map_members_path)
        within "div#displayed_members" do
          expect(page).to have_selector("li", text: m2012.name)
          expect(page).not_to have_selector("li", text: m2013.name)
        end

        within "form#members_filter" do
          check "2013"
          click_button "Filtrer"
        end

        expect(current_path).to eq(map_members_path)
        within "div#displayed_members" do
          expect(page).to have_selector("li", text: m2012.name)
          expect(page).to have_selector("li", text: m2013.name)
        end
      end
    end

  end

  context "as a member" do
    before { login_as_member }

    scenario "I can edit my information" do
      create :menu, title: "Zone membre", uid: "member_zone"
      new_name = "Nouvelle dénomination"
      visit root_path
      within "nav#main-nav" do
        click_link "Ma fiche"
      end

      expect(current_path).to eq(member_path(@member))
      within "section#content" do
        click_edit_link
      end

      expect(current_path).to eq(edit_member_path(@member))
      within "form" do
        fill_in "Dénomination", with: new_name
        click_update_button
      end

      expect(current_path).to eq(member_path(@member))
      within "section#content" do
        expect(page).to have_selector("h1", text: new_name)
      end
    end

    scenario "I can't edit others' information" do
      @other_member = create :member, published: true

      visit member_path @other_member
      within "section#content" do
        expect(page).not_to have_link("Éditer")
      end
    end

    scenario "I can change my password" do
      pass = "secret123$"
      visit edit_member_path @member
      within "form" do
        click_link "Mot de passe"
        within "div#change-password" do
          fill_in "Mot de passe", with: pass
          fill_in "Confirmer le mot de passe", with: pass
        end
        click_update_button
      end
      logout

      login_as @member.login, pass
      expect(page).to have_content "Vous êtes maintenant connecté à la zone membre."
    end

    scenario "I can provide my affiliation number" do
      visit edit_member_path @member
      within "form" do
        fill_in "Numéro d'affiliation", with: "123456"
        click_update_button
      end

      expect(current_path).to eq member_path(@member)
      within "div#public-stuff" do
        expect(page).to have_content "123456"
      end
    end
  end

  context "as a guest" do
    context "regarding a published member" do
      before { @member = create :member, published: true }

      scenario "I can see the public information of its identity card" do
        visit member_path @member
        within "section#content" do
          expect(page).to have_selector("h1", text: @member.name)
          expect(page).to have_selector("div#public-stuff")
        end
      end

      scenario "I cannot see the private information of its identity card" do
        visit member_path @member
        within "section#content" do
          expect(page).not_to have_selector("div#private-stuff")
        end
      end
    end

    scenario "I cannot see the identity card of a private member" do
      member = create :member, published: false
      visit member_path member

      expect(current_path).to eq(root_path)
      expect(page).to have_content "Désolé, vous n'êtes pas autorisé à accéder à cette partie du site."
    end
  end
end
