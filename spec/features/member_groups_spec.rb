#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Member Groups" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      mg = build :member_group
      visit root_path
      within "nav#admin_links" do
        click_link "Membres"
      end
      within "section#content" do
        click_link "Nouveau groupe de membres"
      end

      expect(current_path).to eq(new_member_group_path)
      within "form" do
        fill_in "Intitulé", with: mg.title
        fill_in "Description", with: mg.description
        click_create_button
      end

      expect(current_path).to eq(member_group_path(MemberGroup.last))
      within "section#content" do
        expect(page).to have_content "Le groupe de membres a été créé avec succès."
        expect(page).to have_selector("h1", text: mg.title)
        expect(page).to have_content mg.description
      end

      visit members_path
        within "section#content" do
          expect(page).to have_selector("li", text: mg.title)
      end
    end

    scenario "I can delete one", :js do
      mg = create :member_group
      visit members_path
      within "div#groups_management ul" do
        within "li", text: mg.title do
          click_link mg.title
          within "p.AdminCard-headingActions" do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(members_path)
      within "section#content" do
        expect(page).to have_content "Groupe de membres supprimé avec succès."
        expect(page).not_to have_content mg.title
      end
    end

    context "regarding edition" do
      before { @mg = create :member_group }

      scenario "I can change its info", :js do
        new_title = "Nouveau titre"
        visit members_path
        within "div#groups_management ul" do
          within "li", text: @mg.title do
            click_link @mg.title
            within "p.AdminCard-headingActions" do
              click_edit_link
            end
          end
        end

        expect(current_path).to eq(edit_member_group_path(@mg))
        within "form" do
          fill_in "Intitulé", with: new_title
          click_update_button
        end

        expect(current_path).to eq(member_group_path(@mg))
        within "section#content" do
          expect(page).to have_content "Groupe de membres mis à jour avec succès."
          expect(page).to have_selector("h1", text: new_title)
        end
      end

      scenario "I can associate/disassociate members" do
        m1 = create :member, name: "Membre 1"
        m2 = create :member, name: "Membre 2"
        @mg.members << m1
        visit member_group_path @mg

        within "ul#private_members" do
          expect(page).to have_selector("li", text: m1.name)
          expect(page).not_to have_selector("li", text: m2.name)
        end

        visit edit_member_group_path @mg
        within "form" do
          uncheck m1.name
          check m2.name
          click_update_button
        end

        within "ul#private_members" do
          expect(page).not_to have_selector("li", text: m1.name)
          expect(page).to have_selector("li", text: m2.name)
        end
      end
    end
  end

  context "as a guest" do
    context "members listing" do
      before do
        @member_group = create :member_group
        @settings.map_member_groups << @member_group
        @private_member = create :member, member_groups: [@member_group]
        @published_member = create :member, member_groups: [@member_group], published: true
      end

      scenario "I have all private and published members by default" do
        create :menu, title: "Où pratiquer", uid: 'member_groups'
        visit root_path
        within "nav#main-nav" do
          click_link @member_group.title
        end

        expect(current_path).to eq(member_group_path(@member_group))
        within "ul#published_members" do
          expect(page).to have_link @published_member.name
        end
        within "ul#private_members" do
          expect(page).not_to have_link @private_member.name
          expect(page).to have_content @private_member.name
        end
      end

      scenario "I can filter by filterable activity",:js do
        activity = create :activity
        not_filterable_activity = create :activity, filterable: false
        useless_activity = create :activity
        @published_member.activities << activity
        visit member_group_path @member_group
        click_link "Trier les clubs"
        within "form#members_filter" do
          within "ul#activities_filter" do
            check activity.title
            expect(page).not_to have_content not_filterable_activity.title
            expect(page).not_to have_content useless_activity.title
          end
          click_button "Filtrer"
        end

        expect(current_path).to eq(filter_members_member_group_path(@member_group))
        within "ul#published_members" do
          expect(page).to have_link @published_member.name
        end
        expect(page).not_to have_selector("ul#private_members")
      end

      scenario "I can filter by public type",:js do
        public_type = create :public_type
        @published_member.public_types << public_type
        visit member_group_path @member_group
        click_link "Trier les clubs"
        within "form#members_filter" do
          within "ul#public_types_filter" do
            check public_type.title
          end
          click_button "Filtrer"
        end

        expect(current_path).to eq(filter_members_member_group_path(@member_group))
        within "ul#published_members" do
          expect(page).to have_link @published_member.name
        end
        expect(page).not_to have_selector("ul#private_members")
      end
    end
  end
end
