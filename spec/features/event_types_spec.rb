#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Event Types" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      et = build :event_type
      visit root_path
      within "nav#admin_links" do
        click_link "Evénements"
      end

      expect(current_path).to eq(events_path)
      within "section#content" do
        click_link "Types d'événement"
      end

      expect(current_path).to eq(event_types_path)
      within "section#content" do
        click_link "Nouveau type d'événement"
      end

      expect(current_path).to eq(new_event_type_path)
      within "form" do
        fill_in "Intitulé", with: et.title
        fill_in "Description", with: et.description
        click_create_button
      end

      expect(current_path).to eq(event_type_path(EventType.first))
      within "section#content" do
        expect(page).to have_content "Type d'événement créé avec succès"
        expect(page).to have_selector("h1", text: et.title)
        expect(page).to have_content et.description
      end
    end

    scenario "I can delete one" do
      et = create :event_type
      visit event_types_path
      within "li", text: et.title do
        click_delete_link
      end

      expect(current_path).to eq(event_types_path)
      within "section#content" do
        expect(page).to have_content "Type d'événement supprimé avec succès"
        expect(page).not_to have_content et.title
      end
    end

    context "regarding edition" do
      before { @et = create :event_type }

      scenario "I can change its info" do
        new_title = "Nouvel intitulé"
        visit event_types_path
        within "li", text: @et.title do
          click_edit_link
        end

        expect(current_path).to eq(edit_event_type_path(@et))
        within "form" do
          fill_in "Intitulé", with: new_title
          click_update_button
        end

        expect(current_path).to eq(event_type_path(@et.reload))
        within "section#content" do
          expect(page).to have_content "Type d'événement mis à jour avec succès"
          expect(page).to have_selector("h1", text: new_title)
        end
      end

      scenario "I can select a color for the calendar" do
        color = '#FFC0CB'
        visit edit_event_type_path @et
        within "form" do
          within "div.event_type_color" do
            fill_in "Couleur", with: color
          end
          click_update_button
        end
        visit calendar_path(:year => Time.now.year, :month => Time.now.month)
        within "form#event_types_filter_form" do
          expect(page).to have_selector("li[style=\"background: #{color}\"]", text: @et.title)
        end
      end

      scenario "I can associate it to a GCal calendar" do
        visit edit_event_type_path @et
        within "form" do
          within "div.event_type_should_have_gcal" do
            choose "Oui"
          end
          click_update_button
        end
        within "section#content" do
          expect(page).to have_content "Type d'événement mis à jour avec succès"
        end
      end
    end

    scenario "I can update GA events" do
      # Given
      event_type = create :event_type
      create(:event, begin_at: Time.now, end_at:Time.now, event_type: event_type)
      # When
      visit event_type_path(event_type)
      within "#associated_event" do
        click_on "Créer les événements GA manquants"
      end
      # Then
      expect(current_path).to eq(event_type_path(event_type))
      within "section#content" do
        expect(page).to have_content "La demande de création des événements GA a été lancée."
      end
    end

  end
end
