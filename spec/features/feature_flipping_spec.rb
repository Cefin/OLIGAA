#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.feature "FeatureFlipping", type: :feature do

  context "Platform feature" do

    context "when enabled" do

      before { allow(Features::Platform).to receive(:enabled?).and_return(true) }

      context "as an admin" do

        before { login_as_admin }

        it "displays admin link" do
          visit root_path

          within "nav#admin_links" do
            expect(page).to have_link('Plateforme')
          end
        end
      end

      context "as a participating club" do

        before { login_as_collaborating_club }

        it "displays performances and subscriptions zones on her page" do
          visit member_path(@member)

          within "section#content" do
            expect(page).to have_css("aside#performances")
            expect(page).to have_css("aside#subscriptions")
          end
        end
      end
    end

    context "when disabled" do

      before { allow(Features::Platform).to receive(:enabled?).and_return(false) }

      context "as an admin" do

        before { login_as_admin }

        it "does not display admin link" do
          visit root_path

          within "nav#admin_links" do
            expect(page).not_to have_link('Plateforme')
          end
        end
      end

      context "as a participating club" do

        before { login_as_collaborating_club }

        it "does not display performances and subscriptions zones on her page" do
          visit member_path(@member)

          within "section#content" do
            expect(page).not_to have_css("aside#performances")
            expect(page).not_to have_css("aside#subscriptions")
          end
        end
      end
    end
  end
end
