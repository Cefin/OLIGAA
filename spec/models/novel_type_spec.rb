#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe NovelType do
  describe "associations" do
    it { is_expected.to have_many(:novels) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:novel_type)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
  end

  describe "scopes" do
    it "sorts novel types by ascending title by default" do
      t1 = create :novel_type, title: "B"
      t2 = create :novel_type, title: "A"
      expect(NovelType.all).to eq [t2,t1]
    end
  end

  describe "behaviors" do
  end
end