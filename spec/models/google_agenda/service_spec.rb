#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper.rb'

describe GoogleAgenda::Service do

  subject { GoogleAgenda::Service.instance }

  before(:all) do
    @key = OpenSSL::PKey::RSA.new 2048
  end

  before(:each) do
    @settings.update_attribute :gcal_user, 'test@gcal.org'
    allow(Google::APIClient::KeyUtils).to receive(:load_from_pkcs12)
      .with(File.join(Rails.root, "config", "google-oauth2", "keyfile.p12"), 'notasecret')
      .and_return(@key)
    GoogleAgenda::Service.reset!
  end

  describe ".api_client", :vcr do

    it 'returns an API client' do
      # When
      result = subject.api_client
      # Then
      expect(result).not_to be_nil
    end

    it 'returns the same API client multiple times' do
      first_result = subject.api_client
      # When
      result = subject.api_client
      # Then
      expect(result).to be first_result
    end

  end

  describe ".calendar_api", :vcr do

    it 'returns a calendar API client' do
      # When
      result = subject.calendar_api
      # Then
      expect(result).to respond_to(:calendars)
    end

    it 'returns the same calendar API client multiple times' do
      first_result = subject.calendar_api
      # When
      result = subject.calendar_api
      # Then
      expect(result).to be first_result
    end

  end

end
