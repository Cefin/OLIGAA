#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Member do
  describe "associations" do
    it {is_expected.to have_and_belong_to_many(:activities) }
    it {is_expected.to have_and_belong_to_many(:member_groups) }
    it {is_expected.to have_and_belong_to_many(:public_types) }
    it {is_expected.to have_many(:contacts)}
    it {is_expected.to have_many(:performances)}
    it {is_expected.to have_many(:subscriptions)}
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:member)).to be_valid
    end
    it { is_expected.to validate_presence_of(:login) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:contacts) }
    it { is_expected.to validate_uniqueness_of(:login).ignoring_case_sensitivity }
    it { is_expected.to allow_value("ab-cd.ef@gh_47").for(:login)}
    it { is_expected.not_to allow_value("a$b").for(:login) }
    it { is_expected.not_to allow_value("a b").for(:login) }
    %w(à â ç è é ê î ô ù û).each do |char|
      it { is_expected.not_to allow_value("s#{char}same").for(:login) }
    end
    it { is_expected.to allow_value("").for(:website) }
    it { is_expected.to allow_value("http://www.example.com").for(:website) }
    it { is_expected.to allow_value("https://example.com").for(:website) }
    it { is_expected.not_to allow_value("example.com").for(:website) }
    it { is_expected.not_to allow_value("ftp://example.com").for(:website) }
  end

  describe "scopes" do
    it ".by_name sorts with ascending order on name by default" do
      m1 = create :member, name: "B"
      m2 = create :member, name: "A"
      expect(Member.by_name).to eq [m2, m1]
    end

    it ".admins returns only admin members" do
      m1 = create :member, admin: true
      m2 = create :member, admin: false
      expect(Member.admins).to eq [m1]
    end

    it ".blocked returns only blocked members" do
      m1 = create :member, blocked: true
      m2 = create :member, blocked: false
      expect(Member.blocked).to eq [m1]
    end

    it ".ungrouped returns only members whithin no member group" do
      m1 = create :member
      m2 = create :member
      mg = create :member_group
      mg.members << m1
      expect(Member.ungrouped).to eq [m2]
    end

    it ".with_activities returns members with specific activities" do
      a1 = create :activity
      a2 = create :activity
      a3 = create :activity
      m1 = create :member, name: "A", activities: [a1, a2]
      m2 = create :member, name: "B", activities: [a1]
      m3 = create :member, name: "C", activities: [a3]
      expect(Member.with_activities([a1.id.to_s])).to eq([m1, m2])
      expect(Member.with_activities([a3.id.to_s])).to eq([m3])
      expect(Member.with_activities([a1.id.to_s, a2.id.to_s])).to eq([m1, m2])
    end

    it ".with_public_types returns members with specific public types" do
      pt1 = create :public_type
      pt2 = create :public_type
      pt3 = create :public_type
      m1 = create :member, name: "A", public_types: [pt1, pt2]
      m2 = create :member, name: "B", public_types: [pt1]
      m3 = create :member, name: "C", public_types: [pt3]
      expect(Member.with_public_types([pt1.id.to_s])).to eq([m1, m2])
      expect(Member.with_public_types([pt3.id.to_s])).to eq([m3])
      expect(Member.with_public_types([pt1.id.to_s, pt2.id.to_s])).to eq([m1, m2])
    end

    it ".registry_years returns all the years of existing registry dates sorted ascending" do
      m1 = create :member, registry_date: "01/01/2013"
      m2 = create :member, registry_date: "01/01/2012"
      m3 = create :member, registry_date: "01/01/2012"
      expect(Member.registry_years).to eq ["2012", "2013"]
    end

    it ".of_member_groups returns uniq members for selected member groups" do
      mg1 = create :member_group
      mg2 = create :member_group
      m = create :member, member_groups: [mg1, mg2]
      expect(Member.of_member_groups([mg1.id, mg2.id])).to eq [m]
    end

    it ".for_years returns members where registry date in requested years" do
      m2010 = create :member, registry_date: "01/01/2010"
      m2011 = create :member, registry_date: "01/01/2011"
      m2012 = create :member, registry_date: "01/01/2012"
      m2013 = create :member, registry_date: "01/01/2013"
      expect(Member.for_years(["2011", "2013"])).to eq [m2011, m2013]
    end

    it ".published returns only published members" do
      m1 = create :member, published: true
      m2 = create :member, published: false
      expect(Member.published).to eq [m1]
    end

    it ".not_published returns only not published members" do
      m1 = create :member, published: true
      m2 = create :member, published: false
      expect(Member.not_published).to eq [m2]
    end
  end

  describe "behaviors" do
    it "#admin_label presents the admin status" do
      m = create :member, admin: false
      expect(m.admin_label).to eq("NON")
      m.update_attribute :admin, true
      expect(m.admin_label).to eq("OUI")
    end

    it "#blocked_label presents the blocked status" do
      m = create :member, blocked: false
      expect(m.blocked_label).to eq("NON")
      m.update_attribute :blocked, true
      expect(m.blocked_label).to eq("OUI")
    end

    it "#published_label presents the published status" do
      m = create :member, published: false
      expect(m.published_label).to eq("NON")
      m.update_attribute :published, true
      expect(m.published_label).to eq("OUI")
    end

    it "#localized_status presents the status of map localization" do
      m = create :member, address: ""
      expect(m.localized_status).to eq("Non trouvé sur la carte")
      allow(m).to receive(:geocoded?).and_return(true)
      expect(m.localized_status).to eq("Affiché sur la carte")
    end

    it "login is downcased" do
      m = create :member, login: "Jean-Dupont"
      expect(m.login).to eq("jean-dupont")
    end

    it "saves a contact if new contact type provided" do
      m = create :member
      c = m.contacts.first
      c.new_function = "Nouvelle fonction"
      m.save
      expect(m.reload.contacts.first.contact_type.title).to eq("Nouvelle fonction")
    end

    it "#collaborating_club? returns true if member belong to platform collaborating clubs groups" do
      mg = create :member_group
      m = create :member, member_groups: [mg]
      expect(m).not_to be_collaborating_club
      Settings.instance.reload.platform_collaborating_clubs << mg
      expect(m).to be_collaborating_club
    end

    it "#collaborating_medicosocial_org? returns true if member belongs to platform collaborating medicosocial orgs groups" do
      mg = create :member_group
      m = create :member, member_groups: [mg]
      expect(m).not_to be_collaborating_medicosocial_org
      Settings.instance.reload.platform_collaborating_medicosocial_orgs << mg
      expect(m).to be_collaborating_medicosocial_org
    end

    it "#medicosocial_org? returns true if member belongs to platform medicosocial orgs groups" do
      mg = create :member_group
      m = create :member, member_groups: [mg]
      expect(m).not_to be_medicosocial_org
      Settings.instance.reload.platform_medicosocial_orgs << mg
      expect(m).to be_medicosocial_org
    end

    describe "password management" do
      it "new member has only a default password" do
        m = create :member, :new
        expect(m.password).to be_blank
        expect(m.salt).to be_blank
        expect(m.default_password).not_to be_blank
      end

      it "password can be changed" do
        m = create :member
        m.updating_password = true
        m.password = 'secret'
        m.save!
        expect(m.salt).not_to be_blank
        expect(m.password).not_to be_blank
        expect(m.default_password).to be_blank
      end

      it "password not changed if not necessary" do
        m = create :member
        m.updating_password = nil
        old_salt = m.salt
        old_pass = m.password
        m.password = 'secret'
        m.save! && m.reload
        expect(m.salt).to eq(old_salt)
        expect(m.password).to eq(old_pass)
      end

      it "password can be reset" do
        m = create :member
        m.reset_password
        expect(m.password).to be_blank
        expect(m.salt).to be_blank
        expect(m.default_password).not_to be_blank
      end

      it "salt and password are encrypted" do
        salt = OpenSSL::Digest::SHA1.new(OpenSSL::Random.random_bytes(256)).hexdigest
        expect(salt.length).to eq(40)
        salt = 'salt'
        pass = 'secret'
        m = create :member
        expect(m.hashed_and_salted_password(pass, salt)).to eq("da00ec2e6ff9ed4d342b24a16e262c82f3c8b10b")
      end
    end

    it "exports as CSV" do
      m = create :member, name: "JD"
      c = m.contacts.first
      ct = create :contact_type, title: "Président"
      c.update_attributes({civility: "M.", last_name: "Dupont", first_name: "Jean", email: "jean-dupont@example.com", street: "Rue de la maison", postal_code: "01234", city: "Ma Ville"})
      c.phone_numbers.create({number: "0102030405"})
      c.phone_numbers.create({number: "0605040302"})
      c.update_attribute :contact_type, ct
      mg = create :member_group
      mg.members << m
      expected = <<eos
Membre;Type de contact;Civilité;Last Name;First Name;E-mail;Rue;Code postal;Ville;Tel1;Tel2;Tel3
JD;Président;M.;Dupont;Jean;jean-dupont@example.com;Rue de la maison;01234;Ma Ville;0102030405;0605040302
eos
      expect(Member.all_to_csv(nil)).to eq(expected)
      expect(Member.all_to_csv(mg)).to eq(expected)
    end
  end
end
