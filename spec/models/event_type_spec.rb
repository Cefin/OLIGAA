#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe EventType do
  describe "associations" do
    it {is_expected.to have_many(:events) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:event_type)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
  end

  describe "behaviors" do

    it_behaves_like "seasoned_events"

    describe '.ga_cal' do

      let(:ga_cal_id) { 'calId@google.com' }
      subject { build(:event_type, ga_cal_id: ga_cal_id) }

      it 'returns the found Google calendar' do
        # Given
        ga_cal = double(GoogleAgenda::Calendar)
        expect(GoogleAgenda::Calendar).to receive(:find_by_id_or_create).with(ga_cal_id).and_return(ga_cal)
        # When
        result = subject.ga_cal
        # Then
        expect(result).to be ga_cal
      end

    end

    describe '.save_ga_calendar' do

      let(:ga_cal_id) { 'calId@google.com' }
      subject { create(:event_type) }

      it 'set the ga_cal_id' do
        # Given
        ga_cal = double(GoogleAgenda::Calendar)
        expect(ga_cal).to receive(:update).with(subject).and_return(ga_cal_id)
        expect(GoogleAgenda::Calendar).to receive(:find_by_id_or_create).with(nil).and_return(ga_cal)
        # When
        result = subject.save_ga_calendar_without_delay
        # Then
        expect(result).to be true
        expect(subject.reload.ga_cal_id).to eq ga_cal_id
      end

    end

    describe '.cleanup_ga_events' do

      subject { create(:event_type) }

      it 'detaches associated events and forgets their Google event' do
        # Given
        event1 = create(:event, event_type: subject, ga_evt_id: 'event1@google.com')
        event2 = create(:event, event_type: subject, ga_evt_id: 'event2@google.com')
        # When
        result = subject.cleanup_ga_events
        # Then
        event1 = event1.reload
        expect(event1.ga_evt_id).to be_nil
        expect(event1.event_type).to be_nil
        event2 = event2.reload
        expect(event2.ga_evt_id).to be_nil
        expect(event2.event_type).to be_nil
      end

    end

  end
end
