#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe SubscriptionNumber, type: :model do

  before do
    @member = create :member, affiliation_number: nil
    @subscription = create :subscription, member_id: @member.id, created_at: Date.new(2017,9,19)
    @subscription.update_attribute(:platform_number, nil)
  end

  let(:sn) { SubscriptionNumber.new(@subscription) }

  context "regarding initialization" do
    it "sets @subscription" do
      expect(sn.instance_variable_get(:@subscription)).to eq @subscription
    end
  end

  context "#label" do

    scenario "returns 'Numéro d\'affiliation du membre non renseigné' when affiliation number not filled" do
      expect(sn.label).to eq "Numéro d'affiliation du membre non renseigné"
    end

    context "for member with 3369 as affiliation number" do
      before do
        @member.update_attribute(:affiliation_number, "33/69")
        @subscription.reload
      end

      scenario "returns '3369/1718/001' for the first subscription" do
        expect(sn.label).to eq '3369/1718/001'
      end

      scenario "returns '3369/1718/042' fot the 42nd subscription" do
        latest_subscription = create :subscription, member_id: @member.id
        latest_subscription.update_attribute(:platform_number, '3369/1718/041')
        expect(sn.label).to eq '3369/1718/042'
      end
    end
  end
end
