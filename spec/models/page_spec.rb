#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Page do
  describe "associations" do
    it { is_expected.to belong_to(:menu) }
    it { is_expected.to have_many(:attached_files) }
    it { is_expected.to have_many(:logos) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:page)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:content) }
  end

  describe "scopes" do
    it "sorts the pages by ascending position" do
      menu = create :menu
      page1 = create :page, menu: menu
      page2 = create :page, menu: menu
      page2.move_higher
      expect(Page.all).to eq [page2, page1]
    end
  end

  describe "behaviors" do
    it "#should_be_displayed? returns publication status" do
      expect(build(:page, displayed: false).should_be_displayed?).to eq(false)
      expect(build(:page, displayed: true).should_be_displayed?).to eq(true)
    end

    it "#to_params customizes the URL with id then text" do
      page = create(:page, title: "éèê à ïî ùû ôö $%€")
      expect(page.to_param).to eq("#{page.id}-eee_a_ii_uu_oo_")
    end

    it "changes the position when changing its parent" do
      menu1 = create :menu
      menu2 = create :menu
      page1 = create :page, menu: menu1
      page2 = create :page, menu: menu2
      expect(page1.position).to eq(1)
      expect(page2.position).to eq(1)
      page2.update_attribute(:menu, menu1)
      expect(page1.position).to eq(1)
      expect(page2.reload.position).to eq(2)
    end
  end
end