#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.shared_examples "an unset option" do
  it "#label returns 'Aucune formule disponible pour ce cas'" do
    expect(option.label).to eq 'Aucune formule disponible pour ce cas'
  end
  it "#price is not set" do
    expect(option.price).to be_nil
  end
end

RSpec.shared_examples "Structure médico-sociale" do |partnership, activities_arity, expected_price|
  expected_label = "Structure médico-sociale #{partnership} (#{activities_arity})"

  it "#label returns #{expected_label}" do
    expect(option.label).to eq expected_label
  end
  it "#price returns #{expected_price}" do
    expect(option.price).to eq expected_price
  end
end

RSpec.describe SubscriptionOption, type: :model do
  let(:medicosocial_org) { create :member, member_groups: [create(:member_group)] }
  let(:perf1) { create :performance }
  let(:perf2) { create :performance }

  context "regading initialization" do
    it "sets @medicosocial_org" do
      option = SubscriptionOption.new(medicosocial_org,nil)
      expect(option.instance_variable_get(:@medicosocial_org)).to eq medicosocial_org
    end
    it "sets @performances" do
      option = SubscriptionOption.new(nil,[perf1])
      expect(option.instance_variable_get(:@performances)).to eq [perf1]
    end
  end

  context "formulas" do
    before do
      @settings.mono_activity_collaborating_price = 80
      @settings.mono_activity_standard_price = 90
      @settings.multi_activities_collaborating_price = 120
      @settings.multi_activities_standard_price = 150
      @settings.save!
    end

    context "medicosocial org not set" do

      context "for no activity" do
        let(:option) { SubscriptionOption.new(nil,nil)}
        it_behaves_like "an unset option"
      end

      context "for one activity" do
        before { perf2.update_attribute(:activity, perf1.activity) }
        context "during only one performance" do
          let(:option) { SubscriptionOption.new(nil,[perf1]) }
          it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 90.0
        end

        context "during several performances" do
          let(:option) { SubscriptionOption.new(nil,[perf1, perf2]) }
          context "wihtin the same club" do
            before { perf2.update_attribute(:member, perf1.member) }
            it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 90.0
          end
          context "within several clubs" do
            it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 150.0
          end
        end
      end

      context "for several activities" do
        let(:option) { SubscriptionOption.new(nil,[perf1, perf2]) }
        it_behaves_like "Structure médico-sociale", 'non partenaire', 'plusieurs activités', 150.0
      end
    end

    context "performances not set" do
      let(:option) { SubscriptionOption.new(medicosocial_org,nil)}
      it_behaves_like "an unset option"
    end

    context "regarding collaborating medicosocial org" do
      before do
        @settings.platform_collaborating_medicosocial_orgs << medicosocial_org.member_groups
      end

      context "for one activity" do
        before { perf2.update_attribute(:activity, perf1.activity) }
        context "during only one performance" do
          let(:option) { SubscriptionOption.new(medicosocial_org,[perf1]) }
          it_behaves_like "Structure médico-sociale", 'partenaire', 'une seule activité', 80.0
        end

        context "during several performances" do
          let(:option) { SubscriptionOption.new(medicosocial_org,[perf1, perf2]) }
          context "wihtin the same club" do
            before { perf2.update_attribute(:member, perf1.member) }
            it_behaves_like "Structure médico-sociale", 'partenaire', 'une seule activité', 80.0
          end
          context "within several clubs" do
            it_behaves_like "Structure médico-sociale", 'partenaire', 'une seule activité', 120.0
          end
        end
      end

      context "for several activities" do
        let(:option) { SubscriptionOption.new(medicosocial_org,[perf1, perf2]) }
        it_behaves_like "Structure médico-sociale", 'partenaire', 'plusieurs activités', 120.0
      end
    end

    context "regarding regular medicosocial org" do

      context "for one activity" do
        before { perf2.update_attribute(:activity, perf1.activity) }
        context "during only one performance" do
          let(:option) { SubscriptionOption.new(nil,[perf1]) }
          it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 90.0
        end

        context "during several performances" do
          let(:option) { SubscriptionOption.new(nil,[perf1, perf2]) }
          context "wihtin the same club" do
            before { perf2.update_attribute(:member, perf1.member) }
            it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 90.0
          end
          context "within several clubs" do
            it_behaves_like "Structure médico-sociale", 'non partenaire', 'une seule activité', 150.0
          end
        end
      end

      context "for several activities" do
        let(:option) { SubscriptionOption.new(medicosocial_org,[perf1, perf2]) }
        it_behaves_like "Structure médico-sociale", 'non partenaire', 'plusieurs activités', 150.0
      end
    end
  end
end
