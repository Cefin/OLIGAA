#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Email do
  describe "associations" do
    it { is_expected.to belong_to(:novel) }
    it { is_expected.to belong_to(:member) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:email)).to be_valid
    end
    it { is_expected.to validate_presence_of(:sender) }
    it { is_expected.to validate_presence_of(:sender) }
    describe "regarding recipients" do
      it "is valid with recipients and other recipients" do
        email = build :email
        expect(email).to be_valid
      end
      it "is valid with only recipients" do
        email = build :email, other_recipients: nil
        expect(email).to be_valid
      end
      it "is valid with only other recipients" do
        email = build :email, recipients: nil
        expect(email).to be_valid
      end
      it "is not valid without recipients nor other recipients" do
        email = build :email, recipients: nil, other_recipients: nil
        expect(email).not_to be_valid
      end
    end
  end

  describe "scopes" do
    it "returns emails descending sent_at datetime by default" do
      e1 = create :email, sent_at: Time.now - 2.hours
      e2 = create :email, sent_at: Time.now
      expect(Email.all).to eq [e2, e1]
    end

    it ".unsent only returns unsent emails" do
      e1 = create :email
      e2 = create :email, sent_at: Time.now
      expect(Email.unsent).to eq [e1]
    end

    it ".sent only returns sent emails" do
      e1 = create :email
      e2 = create :email, sent_at: Time.now
      expect(Email.sent).to eq [e2]
    end
  end

  describe "behaviors" do
    it 'sends the e-mail to all recipients' do
      # Given
      subject = create :email, novel: (create :novel)
      # When
      result = subject.send_off
      # Then
      expect(Delayed::Job.count).to eq(3)
      expect(subject.reload.sent_at).not_to be_nil
    end

    context "#member_name" do
      it "returns the name of the member" do
        email = create :email, member: create(:member, name: "Alex")
        expect(email.member_name).to eq("Alex")
      end

      it "returns 'Inconnu' if no member found" do
        email = create :email
        Member.find(email.member.id).destroy
        expect(email.reload.member_name).to eq('Inconnu')
      end
    end

    context "private methods" do
      it "#sanitize_other_recipients returns a comma separated string of manually entered recipients" do
        email = build(:email, other_recipients: "alex@ip.fr, alex@bzh.ip.fr\r\nalex+bzh@infopiiaf.fr\r\nalex")
        expect(email.send(:sanitize_other_recipients)).to eq("alex@ip.fr, alex@bzh.ip.fr, alex+bzh@infopiiaf.fr")
      end

      it "#array_of_recipients splits a comma separated list of recipients to an array" do
        email = build(:email, recipients: "recipient1@example.com, recipient2@example.com")
        expect(email.send(:array_of_recipients)).to eq ["recipient1@example.com", "recipient2@example.com"]
      end
    end

    it "#contacts returns an array of contacts using the recipients" do
      c1 = create :contact, email: "c1@ip.fr"
      c2 = create :contact, email: "c2@ip.fr"
      c3 = create :contact, email: "c1@ip.fr"
      email = create :email, recipients: "c1@ip.fr, c2@ip.fr"
      expect(email.contacts).to eq [c1, c3, c2]
    end

    it "#unused_contacts returns " do
      (c1 = build :contact, email: "c1@ip.fr", member: nil).save(validate: false)
      (c2 = build :contact, email: "c2@ip.fr", member: nil).save(validate: false)
      (c3 = build :contact, email: "c1@ip.fr", member: nil).save(validate: false)
      (email = build :email, recipients: "c1@ip.fr", member: nil).save(validate: false)
      expect(email.unused_contacts).to eq [c2]
    end

    it "#all_recipients returns an array of recipients" do
      email = create :email, recipients: "c1@ip.fr, c2@ip.fr", other_recipients: "c1@ip.fr, c3@ip.fr"
      expect(email.all_recipients).to eq(%w[c1@ip.fr c2@ip.fr c3@ip.fr])
    end
  end
end
