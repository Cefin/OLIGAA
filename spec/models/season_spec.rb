#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Season, type: :model do

  context "regarding initialization" do
    context "when season birthdate not provided" do
      context "sets the beginning ot the season to" do
        scenario "15/08/2017 when date 19/09/2017 is given" do
          season = Season.new(Date.new(2017,9,19))
          expect(season.beginning).to eq Date.new(2017,8,15)
          expect(season.end).to eq Date.new(2018,8,14)
        end
        scenario "15/08/2017 when date 15/08/2017 is given" do
          season = Season.new(Date.new(2017,8,15))
          expect(season.beginning).to eq Date.new(2017,8,15)
          expect(season.end).to eq Date.new(2018,8,14)
        end
        scenario "15/08/2016 when date 14/08/2017 is given" do
          season = Season.new(Date.new(2017,8,14))
          expect(season.beginning).to eq Date.new(2016,8,15)
          expect(season.end).to eq Date.new(2017,8,14)
        end
      end
    end

    context "when season birthdate set to 01/09/1970" do

      before { @settings.update_attribute(:season_birthdate, Date.new(1970,9,1)) }

      context "sets the beginning ot the season to" do
        scenario "01/09/2017 when date 19/09/2017 is given" do
          season = Season.new(Date.new(2017,9,19))
          expect(season.beginning).to eq Date.new(2017,9,1)
          expect(season.end).to eq Date.new(2018,8,31)
        end
        scenario "01/09/2017 when date 01/09/2017 is given" do
          season = Season.new(Date.new(2017,9,1))
          expect(season.beginning).to eq Date.new(2017,9,1)
          expect(season.end).to eq Date.new(2018,8,31)
        end
        scenario "01/09/2016 when date 31/08/2017 is given" do
          season = Season.new(Date.new(2017,8,31))
          expect(season.beginning).to eq Date.new(2016,9,1)
          expect(season.end).to eq Date.new(2017,8,31)
        end
      end
    end
  end

  context "class methods" do

    it ".default_birthdate set to 15/08/1970" do
      # Given
      # When
      result = Season.default_birthdate
      # Then
      expect(result).to eq Date.new(1970,8,15)
    end

    context ".birthdate" do
      it "return 15/08/1970 when season birthdate not provided" do
        # Given
        # When
        result = Season.birthdate
        # Then
        expect(result).to eq Date.new(1970,8,15)
      end

      it "returns season birthdate when provided" do
        # Given
        first_of_september = Date.new(1970,9,1)
        @settings.update_attribute(:season_birthdate, first_of_september)
        # When
        result = Season.birthdate
        # Then
        expect(result).to eq first_of_september
      end
    end

    context ".all returns available seasons" do

      before { allow(Settings).to receive_message_chain(:instance, :reload, :season_birthdate).and_return(Date.new(1970,9,1)) }

      it "returns empty array if none available" do
        # Given
        expected = []
        # When
        result = Season.all(Event)
        # Then
        expect(result).to eq expected
      end

      it "returns only one if all data within same season" do
        # Given
        create :event, begin_at: Date.new(2017,9,1)
        create :event, begin_at: Date.new(2017,12,18)
        create :event, begin_at: Date.new(2018,5,19)
        create :event, begin_at: Date.new(2018,8,31)
        expected = ["2017-2018"]
        # When
        result = Season.all(Event)
        # Then
        expect(result).to eq expected
      end

      it "based on :begin_at if no field provided" do
        # Given
        create :event, begin_at: Date.new(2017,4,1)
        create :event, begin_at: Date.new(2018,3,31)
        expected = ["2016-2017","2017-2018"]
        # When
        result = Season.all(Event)
        # Then
        expect(result).to eq expected
      end

      it "based on the provided field" do
        # Given
        create :subscription, created_at: Date.new(2017,4,1)
        create :subscription, created_at: Date.new(2017,9,1)
        create :subscription, created_at: Date.new(2018,8,31)
        expected = ["2016-2017","2017-2018"]
        # When
        result = Season.all(Subscription, :created_at)
        # Then
        expect(result).to eq expected
      end
    end

    context ".all_for returns items arranged by season" do

      before { allow(Settings).to receive_message_chain(:instance, :reload, :season_birthdate).and_return(Date.new(1970,9,1)) }

      it "based on :begin_at if no field provided" do
        # Given
        Item = Struct.new(:begin_at)
        item1 = Item.new(Date.new(2018,8,31))
        item2 = Item.new(Date.new(2018,9,1))
        item3 = Item.new(Date.new(2018,9,2))
        expected = [
          ["Saison 2018-2019", [item2, item3]],
          ["Saison 2017-2018", [item1]]
        ]
        # When
        result = Season.all_for([ item1, item2, item3 ])
        # Then
        expect(result).to eq expected
      end

      it "based on the provided field" do
        # Given
        Item = Struct.new(:created_at)
        item1 = Item.new(Date.new(2018,8,31))
        item2 = Item.new(Date.new(2018,9,1))
        item3 = Item.new(Date.new(2018,9,2))
        expected = [
          ["Saison 2018-2019", [item2, item3]],
          ["Saison 2017-2018", [item1]]
        ]
        # When
        result = Season.all_for([ item1, item2, item3 ], :created_at)
        # Then
        expect(result).to eq expected
      end
    end

    it ".from_short_label returns a season based on its short label" do
      # Given
      allow(Settings).to receive_message_chain(:instance, :reload, :season_birthdate).and_return(Date.new(1970,9,1))
      short_label = "2017-2018"
      # When
      result = Season.from_short_label(short_label)
      # Then
      expect(result).to be_a Season
      expect(result.beginning).to eq Date.new(2017,9,1)
      expect(result.end).to eq Date.new(2018,8,31)
    end
  end

  scenario "#shorten returns shorten label" do
    season = Season.new(Date.new(2017,9,19))
    expect(season.shorten).to eq "1718"
    season = Season.new(Date.new(2018,9,19))
    expect(season.shorten).to eq "1819"
  end

  scenario "#short_label returns dates description of the season" do
    season = Season.new(Date.new(2017,9,19))
    expect(season.short_label).to eq "2017-2018"
  end

  scenario "#label returns full description of the season" do
    season = Season.new(Date.new(2017,9,19))
    expect(season.label).to eq "Saison 2017-2018"
  end
end
