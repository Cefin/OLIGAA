#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe ContactType do
  describe "validations" do
    it "default factory is valid" do
      expect(build(:contact_type)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_uniqueness_of(:title) }
  end

  describe "associations" do
    it {is_expected.to have_many(:contacts) }
  end

  describe "scopes" do
    it "sorts by ascending title by default" do
      type1 = create(:contact_type, title: "B")
      type2 = create(:contact_type, title: "A")
      expect(ContactType.all).to eq [type2, type1]
    end
  end

  describe "behaviors" do
    it ".all_not_empty only gives contact types associated with contacts" do
      ct1 = create :contact_type
      ct2 = create :contact_type
      contact = create :contact, contact_type: ct1
      expect(ContactType.all_not_empty).to eq [ct1]
    end
  end
end