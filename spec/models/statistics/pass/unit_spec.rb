#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Statistics::Pass::Unit, type: :model do

  let!(:season) { season = Season.new(Date.new(2017,9,1)) }

  context "initialization" do
    it "sets @member" do
      member = create(:member)
      stats = Statistics::Pass::Unit.new(member, season)
      expect(stats.instance_variable_get(:@member)).to eq member
      expect(stats.instance_variable_get(:@season)).to eq season
    end
  end

  context "calculations" do

    context "the member is not a collaborating club nor a collaborating medicosocial org" do
      before do
        member = create :member
        @stats = Statistics::Pass::Unit.new(member, season)
      end

      it "#total returns nil" do
        expect(@stats.total).to eq 0
      end

      it "#multi returns nil" do
        expect(@stats.multi).to eq 0
      end

      it "#mono returns nil" do
        expect(@stats.mono).to eq 0
      end

      it "#label returns nil" do
        expect(@stats.label).to be_nil
      end
    end

    context "the member is a collaborating club" do
      before do
        season
        mg = create :member_group
        Settings.instance.reload.platform_collaborating_clubs << mg
        member = create :member, member_groups: [mg]
        perf1 = create :performance, member: member
        perf2 = create :performance, member: member
        5.times { create :subscription, performances: [perf1], member_id: member.id, created_at: "15/09/2016" }
        5.times { create :subscription, performances: [perf1], member_id: member.id, created_at: "15/09/2017" }
        3.times { create :subscription, performances: [perf1, perf2], member_id: member.id, created_at: "15/09/2016" }
        3.times { create :subscription, performances: [perf1, perf2], member_id: member.id, created_at: "15/09/2017" }
        @stats = Statistics::Pass::Unit.new(member, season)
      end

      it "#total returns the number of its subscriptions" do
        expect(@stats.total).to eq 8
      end

      it "#multi returns the number of its multi activities subscriptions" do
        expect(@stats.multi).to eq 3
      end

      it "#mono returns the number of its mono activity subscriptions" do
        expect(@stats.mono).to eq 5
      end

      it "#label returns '(3 multi, 5 mono sur 8)'" do
        expect(@stats.label).to eq '(3 multi, 5 mono sur 8)'
      end
    end

    context "the member is a medicosocial org" do
      before do
        mg = create :member_group
        Settings.instance.reload.platform_medicosocial_orgs << mg
        club = create :member
        collaborating_medicosocial_org = create :member, member_groups: [mg]
        perf1 = create :performance, member: club
        perf2 = create :performance, member: club
        5.times { create :subscription, adherent: (create :adherent, medico_social_org: collaborating_medicosocial_org), performances: [perf1], member_id: club.id, created_at: "15/09/2016" }
        5.times { create :subscription, adherent: (create :adherent, medico_social_org: collaborating_medicosocial_org), performances: [perf1], member_id: club.id, created_at: "15/09/2017" }
        3.times { create :subscription, adherent: (create :adherent, medico_social_org: collaborating_medicosocial_org), performances: [perf1, perf2], member_id: club.id, created_at: "15/09/2016" }
        3.times { create :subscription, adherent: (create :adherent, medico_social_org: collaborating_medicosocial_org), performances: [perf1, perf2], member_id: club.id, created_at: "15/09/2017" }
        @stats = Statistics::Pass::Unit.new(collaborating_medicosocial_org, season)
      end

      it "#total returns the number of its subscriptions" do
        expect(@stats.total).to eq 8
      end

      it "#multi returns the number of its multi activities subscriptions" do
        expect(@stats.multi).to eq 3
      end

      it "#mono returns the number of its mono activity subscriptions" do
        expect(@stats.mono).to eq 5
      end

      it "#label returns '(3 multi, 5 mono sur 8)'" do
        expect(@stats.label).to eq '(3 multi, 5 mono sur 8)'
      end
    end
  end
end
