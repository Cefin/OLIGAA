#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Mailer do
  describe "send_info" do
    before :each do
      novel_type = create :novel_type, title: "Resultats"
      a1 = create :activity, title: "Athle"
      a2 = create :activity, title: "Football"
      novel = create :novel, title: "Championnats", content: "Voici les...", novel_type: novel_type, activities: [a1, a2]
      email = create :email, sender: "alex@ip.fr", novel: novel
      @mail = Mailer.send_info(email, "recipient1@example.com")
    end

    it "renders the headers" do
      expect(@mail.subject).to eq("Championnats")
      expect(@mail.from).to eq(["alex@ip.fr"])
      expect(@mail.to).to eq(["recipient1@example.com"])
    end

    it "renders the body" do
      expect(@mail.body.encoded).to match("Resultats")
      expect(@mail.body.encoded).to match("Athle / Football")
      expect(@mail.body.encoded).to match("Voici les...")
    end
  end

  pending "TODO: contact_us"
  pending "TODO: reset_password"
end
