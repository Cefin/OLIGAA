#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
fr:
  simple_form:
    "yes": 'Oui'
    "no": 'Non'
    required:
      text: 'obligatoire'
      mark: '*'
      # You can uncomment the line below if you need to overwrite the whole required html.
      # When using html, text and mark won't be used.
      # html: '<abbr title="required">*</abbr>'
    error_notification:
      default_message: "Des erreurs sont apparues, merci de jeter un oeil à :"
    labels:
      contact_us_mail:
        last_name: "Nom"
        first_name: "Prénom"
        email: "Adresse Email"
        phone_number: "Numéro de téléphone"
        subject: "Objet"
        body: "Message"
      attached_file: &attached_file_labels
        name: "Nom"
        description: "Description"
        file: "Fichier"
      image: &image_labels
        title: "Titre"
        description: "Description"
        img_file: "Fichier"
      menu:
        title: "Intitulé"
      page:
        title: "Titre"
        content: "Contenu"
        displayed: "Publique"
        menu_id: "Ajouter au menu"
        attached_files:
          <<: *attached_file_labels
        logos:
          img: "Image"
      activity_category:
        title: "Intitulé"
        description: "Description"
      activity:
        title: "Intitulé"
        presentation: "Présentation"
        description: "Description"
        poster: "Image"
        activity_categories: "Catégories"
        filterable: "Utilisable dans le filtre des membres (cf. carte)"
        listed: "Affichée dans le listing"
        attached_files:
          <<: *attached_file_labels
      event_type:
        title: "Intitulé"
        description: "Description"
        color: "Couleur"
        should_have_gcal: "Associer un calendrier GCal"
      event:
        title: "Intitulé"
        description: "Description"
        begin_at: "Date et heure de début"
        end_at: "Date et heure de fin"
        location: "Lieu"
        price: "Tarif"
        event_type: "Type d'événement"
        activities: "Activités"
        attached_files:
          <<: *attached_file_labels
      contact_us_mail:
        last_name: "Nom"
        first_name: "Prénom"
        email: "Adresse email"
        phone_number: "Numéro de téléphone"
        subject: "Objet"
        body: "Message"
      novel_type:
        title: "Intitulé"
        description: "Description"
      novel:
        title: "Sujet"
        content: "Contenu"
        public: "Publique"
        template: "Modèle"
        novel_type: "Type d'information"
        activities: "Activités"
        picture: "Illustration"
        attached_files:
          <<: *attached_file_labels
      member:
        login: "Identifiant"
        name: "Dénomination"
        description: "Description"
        address: "Adresse postale"
        website: "Site web"
        more_info: "Informations complémentaires"
        other_activities: "Autres activités proposées"
        join_us: "Comment nous rejoindre"
        partners: "Nos partenaires"
        admin: "Administrateur"
        blocked: "Verrouillé"
        published: "Publié"
        password: "Mot de passe"
        password_confirmation: "Confirmer le mot de passe"
        public_types: "Types de publics"
        member_groups: "Groupes de membres"
        activities: "Activités principales"
        registry_date: "Date d'enregistrement"
        affiliation_number: "Numéro d'affiliation"
        contacts:
          last_name: "Nom"
          first_name: "Prénom"
          function: "Fonction"
          email: "Adresse email"
          civility: "Civilité"
          street: "Rue"
          postal_code: "Code postal"
          city: "Ville"
          contact_type: "Fonction"
          phone_numbers:
            number: "Numéro"
            kind: "Type"
      member_group:
        title: "Intitulé"
        description: Description
      public_type:
        title: "Intitulé"
        description: "Description"
      contact_type:
        title: "Intitulé"
      email:
        sender: "Expéditeur"
        recipients: "Destinataires"
        other_recipients: "Autres destinataires"
        sent_at: "Envoyé le"
      partner: &partner_labels
        name: "Nom"
        url: "URL"
        logo: "Ajouter ou modifier le logo"
        group: "Groupe"
      partners_group:
        title: "Nom"
      settings:
        title: "Titre du site"
        site_logo: "Logo site"
        motto: "Slogan du site"
        motto_logo: "Logo slogan"
        home_message: "Message d'accueil"
        contact_email: "Adresse email de contact"
        contact_message: "Message de contact"
        gcal_user: "Identifiant"
        gcal_pass: "Mot de passe"
        newsletters_title: "Titre global"
        partners: "Partenaires par défaut"
        main_color: "Couleur principale"
        secondary_color: "Couleur secondaire"
        h1_color: "Couleur titres de niveau 1"
        h2_color: "Couleur titres de niveau 2"
        map_member_groups: "Groupes de membres à afficher"
        platform_collaborating_clubs: "Clubs partenaires"
        platform_medicosocial_orgs: "Structures medico-sociales"
        platform_collaborating_medicosocial_orgs: "Structures medico-sociales partenaires"
        mono_activity_collaborating_price: "Structure partenaire"
        mono_activity_standard_price: "Structure non partenaire"
        multi_activities_collaborating_price: "Structure partenaire"
        multi_activities_standard_price: "Structure non partenaire"
        season_birthdate: "Changement de saison"
      newsletter:
        title: "Intitulé"
        published: "Publiée"
        items:
          title: "Intitulé"
          content: "Contenu"
          photo: "Ajouter ou modifier la photo"
          attached_files:
            <<: *attached_file_labels
          images:
            <<: *image_labels
      performance:
        day: "Jour de la semaine"
        start_at: "Heure de début"
        end_at: "Heure de fin"
        activity_id: "Activité"
      adherent:
        last_name: "Nom"
        first_name: "Prénom"
        born_on: "Date de naissance"
        permit_number: "N° licence"
        medico_social_org: "Structure médico-sociale"
        other_medicosocial_org: "Autre structure médico-sociale"
      subscription:
        performances: "Séances"
      club_novel:
        title: "Intitulé"
        content: "Contenu"
        published: "Publiée"
        attached_files:
          <<: *attached_file_labels
        images:
          <<: *image_labels
  helpers:
    submit:
      create: "Créer"
      update: "Mettre à jour"
      contact_us_mail:
        create: "Envoyer"
