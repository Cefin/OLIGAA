#!/bin/bash

# Cleanup
rm -f tmp/pids/server.pid

# Start
bundle exec rails s -b 0.0.0.0 -p 3000
