class AddClubNovelsMenu < ActiveRecord::Migration[5.1]
  def up
    Menu.find_or_create_by(uid: 'club_novels') do |menu|
      menu.title = "Info des clubs Sport Adapté Girondins"
    end
  end

  def down
    Menu.find_by(uid: 'club_novels').destroy
  end
end
