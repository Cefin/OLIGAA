class AddSeasonBirthdateToWebsites < ActiveRecord::Migration[5.1]
  def change
    add_column :websites, :season_birthdate, :date
  end
end
