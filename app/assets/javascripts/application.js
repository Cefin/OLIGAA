/*
 * OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
 * des Associations" which is an online tool for associations to manage news,
 * actors and activities.
 * Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
 * 33) <contact@cdsa33.org>
 *
 * This file is part of OLIGAA.
 *
 * OLIGAA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OLIGAA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
 */
//= require jquery
//= require jquery_ujs
//= require modernizr-custom
//= require html5shiv
//= require autoresize.jquery.min
//= require jquery_nested_form
//= require jquery-ui/effect
//= require jquery-ui/widgets/tabs
//= require jquery-ui/widgets/accordion
//= require jquery-ui/widgets/dialog
//= require jquery-ui/widgets/datepicker
//= require jquery-ui/i18n/datepicker-fr
//= require jquery-ui/widgets/slider
//= require jquery-ui-timepicker-addon
//= require jquery-ui-timepicker-fr
//= require jquery.highlight
//= require jquery.switchButton
//= require fitvids.js
//= require underscore-min
//= require leaflet
//= require gretel.trails.hidden
//= require prettyphoto-rails
//= require moment
//= require fullcalendar
//= require fullcalendar/lang/fr
//= require calendar
//= require jquery.minicolors
//= require novels
//= require clipboard
//= require subscriptions
//= require_self

$(document).ready(function() {

  manageClipboard();

  prepareExternalLinks('a[class~=externe]');
  openExternalLink('a[class~=externe]');
  $('div.tabs').tabs({
  	activate: display_selected_contacts()
  });
  $('div.accordion').accordion({
		collapsible: true,
		heightStyle: "content",
    create: function( event, ui ) {
      var activated = ($(this).data('accordion-active') == false ? false : 0)
      $(this).accordion("option", "active", activated)
    }
	});
  $('textarea').autoResize({
      // On resize:
      onResize : function() {
          $(this).css({opacity:1});
      },
      // After resize:
      animateCallback : function() {
          $(this).css({opacity:1});
      },
      // Quite slow animation:
      animateDuration : 300,
      // More extra space:
      extraSpace : 20
  });
	$('div[data-expand]').each(function() {
		expand_path(this);
	});
	$('input[data-behaviour="datepicker"]').each(function() {
		$(this).datetimepicker({
      changeMonth: true,
      changeYear: true,
      stepMinute: 5,
      showWeek: true,
      showOtherMonths: true,
      selectOtherMonths: true
    });
	});
  $('input[data-behaviour="timepicker"]').each(function() {
    $(this).timepicker({
    });
  });
  $("section#content").fitVids();
  // Image lightbox
  $('a[rel^=prettyPhoto]').prettyPhoto({
    social_tools: '',
    slideshow: 5000,
    autoplay_slideshow: true,
    overlay_gallery: false
  });

  resizeHomeNovels();

  $("#toggleMenu").click(function (event) {
    var $columns = $("#columns")
    var menuShownClass = 'menu-shown'

    $columns.toggleClass(menuShownClass);
  });

  color_input_alternative();

  $('a').on("click", ".expendable", function(event) {
    event.preventDefault();
    toggle_fields($(this).data("field"));
    return false;
  });

  $('a.expendable').on("click", function(event) {
    toggle_link($(this), event);
  });

  $('a.fetchable').click(function(event) {
    event.preventDefault();
    fetch_or_toggle_members($(this).data("field"));
    return false;
  });

  $('a.checkable').on("click", function(event) {
    check_link($(this), event);
  });

  configure_calendar();
});

$(document).on('nested:fieldAdded', function(event){
  $('div.tabs').tabs();
})

function manageClipboard() {
  var clipboard = new Clipboard('.clipboard-btn');

  clipboard.on('success', function(e) {
    alert('Copié !');
  });
}

function toggle_link(link, event) {
  event.preventDefault();
  toggle_fields(link.data("field"));
  return false;
}

function check_link(link, event) {
  event.preventDefault();
  field = link.data("field");
  action = link.data("check-action");
  after_action = link.data("after-action")
  if ( action == "check") {
    check_all(field);
  } else {
    uncheck_all(field);
  }
  switch (after_action) {
    case "expand":
      expand_field(field);
      $(field).find('a.expendable').each(function(i) {
        expand_field($(this).data("field"));
      });
      break;
    default:
    case "close":
      close_field(field);
      break;
  }
  return false;
}

function remove_fields (link, css_class) {
	var hidden_field = $(link).prev('input[type=hidden]')[0];
	if(hidden_field) {
	  hidden_field.value = '1';
	}
	$(link).closest(css_class).hide();
	return false;
}

function add_fields (link, association, content) {
	var new_id = new Date().getTime();
	var regexp = new RegExp("new_" + association, "g");
	$(link).parent().before(content.replace(regexp, new_id));
	return false;
}

function toggle_fields (id) {
	$(id).fadeToggle("fast");
}

function expand_field (id) {
	$(id).fadeIn("fast");
}

function close_field (id) {
	$(id).fadeOut("fast");
}

function toggle_here (e, id) {
	toggle_fields($(e).next(id));
}

function prepareExternalLinks(selector) {
  $.each($(selector), function(index, item) {
    $(item).attr("title", "ouverture dans une nouvelle fenêtre")
  });
}

function openExternalLink (selector) {
  $(selector).click(function(e){
      if(this.href){
          window.open(this.href);
          e.preventDefault();
      }
  });
}

/* Recipients management */
function check_all (id) {
	$(id+' input[type=checkbox]').each(function() {
		this.checked = true;
	});
}

function uncheck_all (id) {
	$(id+' input[type=checkbox]').each(function() {
		this.checked = false;
	});
}

function display_selected_contacts () {
	$('li:has(input:checked)').each(function() {
		expand_field("#" + $(this).parent().attr("id"));
	});
}

function display_selected_contacts_in ($container) {
  $container.find('li li:has(input:checked)').each(function() {
    expand_field("#" + $(this).parent().attr("id"));
  });
}

/* Members management */
function expand_path (e) {
	path = $(e).attr('data-expand');
	tab = path.split("|")[0].slice(1);
	mg = path.split("|")[1].slice(0, -1);
	fetch_or_toggle_members(mg);
}

function fetch_or_toggle_members (mg) {

	var members_zone = $(mg)
  var global_zone = members_zone.parent("div")
	if ( mg.split("-")[1] == undefined ) {
		var member_group_id = mg.split("#")[1]
	} else {
		var member_group_id = mg.split("-")[1]
	}

	if ( members_zone.children().length > 0 ) { // should test if number of sub-elements > 0
		toggle_fields(global_zone);
	} else {
		$.ajax({
			type: 'GET',
			url: '/member_groups/' + member_group_id + '/members',
			dataType: 'html'
		})
		.done(function (data) {
			members_zone.append(data);
			global_zone.show();
		})
		.fail(function () { alert("Une erreur s'est produite. N'hésitez as à essayer de nouveau plus tard et si le problème persiste à contacter infoPiiaf ;)") })
	}
}

function color_input_alternative () {
  if (!Modernizr.inputtypes.color) {
    $('input[type=color]').minicolors({
      colorFormat: '#HEX'
    });
  }
}
