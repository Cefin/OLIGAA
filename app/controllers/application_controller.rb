#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class ApplicationController < ActionController::Base
  protect_from_forgery

  helper :all
  helper_method :admin?, :current_member, :settings

  before_action :set_timezone, :check_new_member

  def set_timezone
    Time.zone = "Paris"
  end

  def check_new_member
    if current_member && session[:must_change_password]
      redirect_to edit_member_path(current_member), notice: "Vous devez modifier votre mot de passe avant toute chose !"
    end
  end

  protected

  def current_member
    session[:member_id] ? @current_member ||= Member.find(session[:member_id]) : nil
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def settings
    Settings.instance.reload
  end

  def authorize
    unless  admin?
      flash[:error] = "Désolé, vous n'êtes pas autorisé à accéder à cette partie du site."
      redirect_to root_path
      false
    end
  end

  def admin?
    if current_member
      current_member.admin?
    else
      false
    end
  end

  def google_marker(color)
    {
      url: "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=°|#{color}|000000",
      width: 32,
      height: 32
    }
  end

  def attached_file_permitted_attributes
    [ :id, :name, :description, :file, :_destroy ]
  end

  def image_permitted_attributes
    [ :id, :title, :description, :img_file, :_destroy ]
  end

  def novel_permitted_attributes
    [ :id, :novel_type_id, :title, :content, :public, :template, :picture,
      activity_ids: [], attached_files_attributes: attached_file_permitted_attributes ]
  end

end
