#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PerformancesController < ApplicationController
  include AuthorizationsHelper

  before_action :set_member
  before_action :set_performance, only: [:edit, :update, :toggle,  :destroy]
  before_action :authorize_member
  before_action :set_available_days
  before_action :set_activities


  def new
    @performance = @member.performances.build
  end

  def create
    @performance = @member.performances.build(performance_params)

    if @performance.save
      redirect_to @member, notice: 'La séance a bien été créée.'
    else
      flash.now[:error] = t('flash.error')
      render :new
    end
  end

  def edit
  end

  def update
    if @performance.update_attributes(performance_params)
      redirect_to @member, notice: 'Séance mise à jour avec succès.'
    else
      flash.now[:error] = t('flash.error')
      render :edit
    end
  end

  def toggle
    @performance.toggle(:disabled)
    done_action = (@performance.disabled? ? "désactivée" : "activée")
    if @performance.save
      redirect_to @member, notice: "Séance #{done_action} avec succès."
    else
      redirect_to @member, error: "La séance n'a pu être #{done_action}."
    end
  end

  def destroy
    @performance.destroy
    redirect_to @member, notice: 'Séance supprimée avec succès.'
  end

  private

  def set_member
    @member = Member.find(params[:member_id])
  end

  def set_performance
    @performance = @member.performances.find(params[:id])
  end

  def authorize_member
    unless can_manage_performances?(@member)
      flash[:error] = "Vous nêtes pas autorisé à accéder à cette zone"
      redirect_to root_path
    end
  end

  def performance_params
    params.require(:performance).permit(:member_id, :day, :start_at, :end_at, :activity_id)
  end

  def set_available_days
    @days = t('date.day_names').zip Date::DAYNAMES.map(&:downcase)
  end

  def set_activities
    @activities = @member.activities
  end
end
