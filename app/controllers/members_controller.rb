#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class MembersController < ApplicationController
  before_action :authorize, except: [:show, :edit, :update]
  skip_before_action :check_new_member, only: [:edit, :update]

  def index
    respond_to do |format|
      format.html do
        @path_to_expand = if params[:mg_id] && params[:mg_id] != "0"
          "#groups_management|" << (params[:mg_id] == "ungrouped" ? "#ungrouped" : "#mg-#{params[:mg_id]}")
        end
        @member_groups = MemberGroup.by_title
      end
      format.csv do
        response.headers["Cache-Control"] = "no-cache, no-store"
        member_group = MemberGroup.find(params[:mg_id]) if params[:mg_id]
        filename = "#{member_group ? member_group.title.gsub(/[\\\/:"*?<>|!]/, '') : 'Contacts'}_#{Time.now.localtime.strftime('%Y-%m-%d_%H%M%S')}.csv"
        send_data Member.all_to_csv(member_group).encode('ISO-8859-15', undef: :replace, invalid: :replace, replace: "?"),
          :type => 'text/csv; charset=iso8859-1; header=present',
          :filename => filename,
          :disposition => "attachment"
      end
    end
  end

  def map
    @member_groups = MemberGroup.all
    @registry_dates = Member.registry_years
    base_members = Member.localized
    base_members = base_members.of_member_groups(params[:member_groups]) if params[:member_groups]
    base_members = base_members.for_years(params[:registry_dates]) if params[:registry_dates]
    @members = (params[:member_groups] || params[:registry_dates]) ? base_members : []
    if @members.empty?
      @markers = []
      flash[:alert] = "Aucun élément n'a été trouvé"
    else
      @legend = {}
      colors = COLORS.dup
      default_color = :grey
      if params[:member_groups]
        requested_groups = MemberGroup.find(params[:member_groups])
        requested_groups.each do |group|
          @legend[group.id] = {name: group.title, color: (colors.empty? ? default_color : colors.shift)}
        end
      else
        requested_groups = []
      end
      @markers = @members.map do |member|
        color = if !member.member_groups.empty? && (mg = (member.member_groups & requested_groups).first) && @legend[mg.id].present?
          @legend[mg.id][:color]
        else
          default_color
        end
        {
          latlng: [member.latitude, member.longitude],
          title: member.name,
          popup: render_to_string(partial: "/members/infowindow", locals: { member: member}),
          icon: color
        }
      end
    end
  end

  def search_contacts
    @search = params[:contacts_search]
    @members = Member.with_contacts(@search)
    redirect_to members_path, alert: "Aucun contact n'a été trouvé contenant '#{@search}' dans son nom, son prénom ou son adresse email." if @members.empty?
  end

  def show
    member = Member.includes(:contacts).where(id: params[:id]).first
    if !member.nil? && member.published? || member == current_member || admin?
      @member = member
      @enabled_performances = @member.performances.enabled
      @disabled_performances = @member.performances.disabled
      if @member.collaborating_club?
        @internal_subscriptions = @member.subscriptions
        @seasoned_internal_subscriptions = Season.all_for(@internal_subscriptions, :created_at)
        @external_subscriptions = Subscription.for_performances(@member.performance_ids) - @internal_subscriptions
        @seasoned_external_subscriptions = Season.all_for(@external_subscriptions, :created_at)
      elsif @member.collaborating_medicosocial_org?
        @internal_subscriptions = Subscription.of_medicosocial_org(@member).by_adherent_name
        @seasoned_internal_subscriptions = Season.all_for(@internal_subscriptions, :created_at)
      end
    else
      authorize
    end
  end

  def new
    @member = Member.new(admin: false, published: false, blocked: false)
    @member.contacts.build
  end

  def create
    @member = Member.new(member_params)
    @member.create_default_password
    if @member.save
      flash[:notice] = "Le compte a maintenant été créé."
      redirect_to members_path
    else
      flash[:error] = t('flash.error')
      render :action => 'new'
    end
  end

  def edit
    if @member = sanitized_member
      @mg_id = params[:mg_id] if params[:mg_id]
      @member.contacts.build if @member.contacts.empty?
    else
      flash[:error] = "Vous nêtes pas autorisé à accéder à cette zone"
      redirect_to root_path
    end
  end

  def update
    @member = sanitized_member
    @error = false
    if params[:member][:password] && !params[:member][:password].empty?
      if params[:member][:password] == params[:member][:password_confirmation]
        @member.updating_password = true
        session.delete(:must_change_password)
      else
        flash.now[:error] = "Assurez-vous d'avoir bien rentré deux fois le même mot de passe!"
        @error = true
      end
    end
    if @member.update_attributes(member_params)
      flash[:notice] = 'Compte mis à jour avec succès.' unless @error
    else
      @error = true
    end
    if @error
      @mg_id = params[:selected_mg] if params[:selected_mg]
      flash[:error] = t('flash.error')
      render :action => "edit"
    else
      if admin?
        redirect = (params[:selected_mg].present? ? members_url(mg_id: params[:selected_mg]) : @member)
        redirect_to redirect
      else
        redirect_to @member
      end
    end
  end

  def raz_pass
    @member = sanitized_member
    if @member.reset_password
      flash[:notice] = "Mot de passe du membre remis à zéro avec succès."
      success, errors = [], []
      @member.contacts.each do |contact|
        begin
          Mailer.reset_password(@member, contact.email).deliver_later
          success << contact.email
        rescue Exception => e
          errors << "#{contact.email} (#{e.message})"
        end
      end
      flash[:notice] << " Email envoyé avec succès à : #{success.join(', ')}."
      flash[:error] = "Erreur lors de l'envoi du mail de notification à : #{errors.join(', ')}" unless errors.empty?
      redirect_to(members_url)
    end
  end

  def destroy
    if current_member.id.to_s != params[:id]
      @member = Member.find(params[:id])
      @member.destroy
      flash[:notice] = 'Compte supprimé avec succès.'
    else
      flash[:error] = 'Vous ne pouvez pas vous supprimer, il faut demander à un autre adminitrateur de le faire pour vous ;)'
    end
    redirect_to(members_url)
  end

  private

  def sanitized_member
    if current_member
      current_member.admin? ? Member.includes(:contacts).where(id: params[:id]).first : current_member
    else
      nil
    end
  end

  def member_params
    permitted_attributes = [
      :name, :login, :other_activities, :description, :address, :affiliation_number,
      :website, :join_us, :partners, :more_info, :password, :registry_date,
      public_type_ids: [], activity_ids: [],
      contacts_attributes: [ :id, :civility, :last_name, :first_name, :contact_type_id, :new_function,
        :email, :street, :postal_code, :city, :_destroy,
        phone_numbers_attributes: [ :id, :number, :kind, :_destroy ]
      ]
    ]
    permitted_attributes << [:admin, :published, :blocked, member_group_ids: []] if current_member.admin?
    params.require(:member).permit(permitted_attributes.flatten)
  end

end
