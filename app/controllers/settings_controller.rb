#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class SettingsController < ApplicationController
  before_action :authorize

  def edit
    @settings = settings
    @partners = Partner.by_name
  end

  def update
    @settings = settings
    if params[:settings][:_destroy_site_logo] == "1"
      @settings.site_logo.destroy
    end
    if params[:settings][:_destroy_motto_logo] == "1"
      @settings.motto_logo.destroy
    end
    if @settings.update_attributes(settings_params)
      reset_ga_service
      redirect_to edit_settings_path, notice: "Les paramètres ont bien été mis à jour"
    else
      reset_ga_service
      flash[:error] = t('flash.error')
      render :edit
    end
  end

  private

  def reset_ga_service
    GoogleAgenda::Service.reset!
  end

  def settings_params
    params.require(:settings).permit(:contact_email, :gcal_pass, :gcal_user, :home_message, :motto, :title,
      :site_logo, :motto_logo, :newsletters_title, :contact_message, :season_birthdate,
      :main_color, :secondary_color, :h1_color, :h2_color,
      :mono_activity_collaborating_price, :mono_activity_standard_price,
      :multi_activities_collaborating_price, :multi_activities_standard_price,
      map_member_group_ids: [], partner_ids: [],
      platform_collaborating_club_ids: [], platform_medicosocial_org_ids: [], platform_collaborating_medicosocial_org_ids: [])
  end
end
