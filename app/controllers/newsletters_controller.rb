#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class NewslettersController < ApplicationController
  before_action :authorize, except: [:index, :show]

  def index
    @newsletters = admin? ? Newsletter.ordered : Newsletter.ordered.published
    @for_nav = Newsletter.for_nav
  end

  def new
    @newsletter = Newsletter.new({partner_ids: settings.partners.map(&:id)})
    prepare_partners
  end

  def create
    @newsletter = Newsletter.new(newsletter_params)
    if @newsletter.save
      schedule_pdf
      redirect_to @newsletter
    else
      render :new
    end
  end

  def show
    @newsletter = Newsletter.find(params[:id])
    @partners = @newsletter.partners
    if admin? || @newsletter.published?
      respond_to do |format|
        format.html
        format.pdf do
          if @newsletter.has_pdf?
            send_file @newsletter.pdf_path, type: 'application/pdf'
            expires_now
          else
            flash[:alert] = "Le PDF n'a pas encore été généré. Merci de revenir plus tard."
            redirect_to @newsletter
          end
        end
      end
    else
      authorize
    end
  end

  def edit
    @newsletter = Newsletter.find(params[:id])
    prepare_partners
  end

  def update
    @newsletter = Newsletter.find(params[:id])
    if @newsletter.update_attributes(newsletter_params)
      schedule_pdf
      redirect_to @newsletter
    else
      render :edit
    end
  end

  def destroy
    Newsletter.find(params[:id]).destroy
    redirect_to newsletters_path
  end

  private

  def prepare_partners
    @partners = Partner.by_name
  end

  def schedule_pdf
    if @newsletter.published?
      File.delete(@newsletter.pdf_path) if @newsletter.has_pdf?
      Delayed::Job.enqueue(PdfJob.new(newsletter_url(@newsletter), @newsletter.pdf_filename))
      flash[:notice] = "La génération du PDF a été planifiée."
    end
  end

  def newsletter_params
    params.require(:newsletter).permit(:title, :published, partner_ids: [],
      items_attributes: [
        :id, :content, :newsletter_id, :title, :photo, :delete_photo, :_destroy, activity_ids: [],
        images_attributes: image_permitted_attributes,
        attached_files_attributes: attached_file_permitted_attributes
        ])
  end
end
