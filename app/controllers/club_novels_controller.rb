#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class ClubNovelsController < ApplicationController
  before_action :authorize, except: [:index]

  def index
    @title = Menu.find_by(uid: 'club_novels')&.title
    @unpublished_club_novels = ClubNovel.unpublished.paginate(page: params[:page])
    @published_club_novels = ClubNovel.published.ordered.paginate(page: params[:page])
  end

  def new
    @club_novel = ClubNovel.new
  end

  def create
    @club_novel = ClubNovel.new(club_novel_params)
    sanitize_publication
    if @club_novel.save
      flash[:notice] = 'Info club créée avec succès.'
      redirect_to club_novels_path
    else
      render :new
    end
  end

  def edit
    @club_novel = ClubNovel.find(params[:id])
  end

  def update
    @club_novel = ClubNovel.find(params[:id])
    sanitize_publication
    if @club_novel.update_attributes(club_novel_params)
      flash[:notice] = 'Info club modifiée avec succès.'
      redirect_to club_novels_path
    else
      render :edit
    end
  end

  def destroy
    ClubNovel.find(params[:id]).destroy
    flash[:notice] = 'Info club supprimée avec succès.'
    redirect_to club_novels_path
  end

  private

  def club_novel_params
    params.require(:club_novel).permit(:title, :content, :published_at, activity_ids: [],
      attached_files_attributes: attached_file_permitted_attributes,
      images_attributes: image_permitted_attributes)
  end

  def sanitize_publication
    @club_novel.published_at = (params[:club_novel][:published] == "true" ? Time.zone.now : nil)
    params[:club_novel].delete(:published)
  end
end
