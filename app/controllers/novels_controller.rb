#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class NovelsController < ApplicationController
  before_action :authorize, :except => [:public_index, :show, :feed]

  def public_index
    @novels = Novel.public_ones.without_templates.paginate(page: params[:page])
  end

  def feed
    @novels = Novel.public_ones
    respond_to do |format|
      format.atom
    end
  end

  def index
    @templates = Novel.templates
    @novel_types = NovelType.all
    @untyped_novels = Novel.untyped.without_templates
  end

  def show
    @novel = Novel.find(params[:id])
    authorize unless @novel.public?
  end

  def duplicate
    @novel = Novel.duplicate(params[:id])
    render "edit"
  end

  def new
    @novel = Novel.new
  end

  def edit
    @novel = Novel.find(params[:id])
  end

  def create
    @novel = Novel.new(novel_params)
    if @novel.save
      flash[:notice] = 'Information créée avec succès.'
      redirect_to(@novel)
    else
      flash[:error] = t('flash.error')
      render "new"
    end
  end

  def update
    @novel = Novel.find(params[:id])
    if params[:novel][:_destroy] == "1"
      @novel.picture.destroy
    end
    if @novel.update_attributes(novel_params)
      flash[:notice] = 'Information mise à jour avec succès.'
      redirect_to(@novel)
    else
      flash[:error] = t('flash.error')
      render "edit"
    end
  end

  def destroy
    @novel = Novel.find(params[:id])
    @novel.destroy
    flash[:notice] = 'Information supprimée avec succès.'
    redirect_to(novels_url)
  end

  private

  def novel_params
    params.require(:novel).permit(novel_permitted_attributes)
  end

end
