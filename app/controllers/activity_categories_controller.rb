#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class ActivityCategoriesController < ApplicationController
  before_action :authorize, :except => [ :show ]

  def show
    @activity_category = ActivityCategory.find(params[:id])
    @activities = @activity_category.activities.listed
  end

  def new
    @activity_category = ActivityCategory.new
  end

  def edit
    @activity_category = ActivityCategory.find(params[:id])
  end

  def create
    @activity_category = ActivityCategory.new(activity_category_params)
    if @activity_category.save
      flash[:notice] = 'La catégorie d\'activité a été créée avec succès.'
      redirect_to @activity_category
    else
      render :action => "new"
    end
  end

  def update
    @activity_category = ActivityCategory.find(params[:id])
    if @activity_category.update_attributes(activity_category_params)
      flash[:notice] = 'La catégorie d\'activité a été modifiée avec succès.'
      redirect_to @activity_category
    else
      render :action => "edit"
    end
  end

  def destroy
    @activity_category = ActivityCategory.find(params[:id])
    @activity_category.destroy
    flash[:notice] = 'La catégorie d\'activité a été supprimée avec succès.'
    redirect_to activities_url
  end

  private

  def activity_category_params
    params.require(:activity_category).permit(:title, :description)
  end
end
