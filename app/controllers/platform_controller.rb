# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PlatformController < ApplicationController
  before_action :authorize

  def index
    if session[:season] = params[:season]
      @season = Season.from_short_label(params[:season])
      @collaborating_clubs = Member.of_member_groups(settings.platform_collaborating_clubs).by_name
      @orgs_in_db_by_name = Member.of_member_groups(settings.platform_medicosocial_orgs).by_name
      @orgs_in_db = {}
      @orgs_in_db_by_name.each do |org|
        subscriptions = Subscription.for_season(@season).of_medicosocial_org(org).by_adherent_name.all
        @orgs_in_db[org] = subscriptions unless subscriptions.empty?
      end
      @orgs_not_in_db = {}
      Adherent.where.not(other_medicosocial_org: nil).group(:other_medicosocial_org).order(:other_medicosocial_org).count.each do |org_name, result|
        @orgs_not_in_db[Member.new({name: org_name})] = Subscription.for_season(@season).joins(:adherent).where(adherents: {other_medicosocial_org: org_name}).by_adherent_name.all
      end
    end
  end
end
