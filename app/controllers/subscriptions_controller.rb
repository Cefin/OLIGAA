#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class SubscriptionsController < ApplicationController
  include AuthorizationsHelper

  before_action :set_member
  before_action :set_subscription, only: [:edit, :update, :destroy, :card, :toggle_billing, :renew]
  before_action :authorize_member, except: [:destroy, :toggle_billing, :card]
  before_action :authorize, only: [:destroy, :toggle_billing]
  before_action :set_medico_social_orgs, except: [:destroy, :card, :toggle_billing]
  before_action :set_available_performances, except: [:destroy, :card, :toggle_billing]

  def new
    @subscription = @member.subscriptions.build
    @subscription.build_adherent
  end

  def create
    @subscription = @member.subscriptions.build(subscription_params).tap do |subscription|
      subscription.adherent = Adherent.available_for_current_season(adherent_params)
    end

    if @subscription.save
      @subscription.external_clubs.each do |external_club|
        failed_recipients = []
        begin
          SubscriptionsMailer.notify_collaborating_club(@subscription, external_club).deliver_now
        rescue Exception => e
          failed_recipients << external_club.name
        end
        unless failed_recipients.empty?
          flash[:warning] = "Une erreur a été rencontrée lors de l'envoi du courriel aux clubs suivants : #{failed_recipients.join(', ')}."
        end
      end
      redirect_to @member, notice: "L'adhésion de #{@subscription.adherent.name} a bien été créée. Pour modifier cette adhésion merci de vous mettre en relation avec un administrateur du site afin que ce dernier procède aux changements."
    else
      flash.now[:error] = t('flash.error')
      render :new
    end
  end

  def edit
  end

  def update
    old_performance_ids = @subscription.performance_ids
    new_performance_ids = global_params[:performance_ids].reject { |id| id == "" }.map(&:to_i)
    intersection = old_performance_ids & new_performance_ids
    removed_performance_ids = old_performance_ids - intersection
    members_concerned_for_removal = Member.joins(:performances).where(performances: { id: removed_performance_ids }).uniq
    added_performance_ids = new_performance_ids - intersection
    members_concerned_for_addition = Member.joins(:performances).where(performances: { id: added_performance_ids }).uniq
    if @subscription.update_attributes(global_params)
      notify_clubs_concerned_by_removal(members_concerned_for_removal, removed_performance_ids)
      notify_clubs_concerned_by_addition(members_concerned_for_addition, added_performance_ids)
      if @failed_recipients.present?
        flash[:warning] = "Une erreur a été rencontrée lors de l'envoi du courriel aux clubs suivants : #{@failed_recipients.join(', ')}."
      end
      redirect_to @member, notice: "L'adhésion a bien été mise à jour."
    else
      flash[:error] = t('flash.error')
      render :edit
    end
  end

  def option
    medicosocial_org = Member.find_by_id(params[:medicosocial_org_id])
    performances = Performance.where(id: params[:performance_ids]).to_a
    @option = SubscriptionOption.new(medicosocial_org,performances)
    render layout: nil
  end

  def renew
    old_subscription = @subscription
    @subscription = old_subscription.dup
    @subscription.performance_ids = old_subscription.performance_ids
  end

  def destroy
    if @subscription
      @subscription.destroy
      redirect_to @member, notice: "L'adhésion a bien été supprimée."
    end
  end

  def card
    respond_to do |format|
      format.html do
        render :card, layout: false
      end
      format.pdf do
        pdf = PDFKit.new(card_member_subscription_url(@member, @subscription), margin_left: 3, margin_right: 3).to_pdf
        send_data pdf, filename: "Carte adhésion #{@subscription.adherent.name} - #{@subscription.season.label}.pdf", type: 'application/pdf'
        expires_now
      end
    end
  end

  def toggle_billing
    @subscription.toggle_billing
    redirect_to platform_path(season: session[:season]), notice: "L'adhésion a bien été #{@subscription.billed_at ? '' : 'dé'}facturée."
  end

  private

  def set_member
    @member = Member.find(params[:member_id])
  end

  def set_subscription
    @subscription = @member.subscriptions.find_by_id(params[:id])
  end

  def authorize_member
    unless can_manage_subscriptions?(@member)
      flash[:error] = "Vous nêtes pas autorisé à accéder à cette zone"
      redirect_to root_path
    end
  end

  def global_params
    params.require(:subscription).permit(:member_id,
      adherent_attributes: [:id, :first_name, :last_name, :born_on, :permit_number, :medico_social_org_id, :other_medicosocial_org],
      performance_ids: [])
  end

  def subscription_params
    global_params.except(:adherent_attributes)
  end

  def adherent_params
    global_params[:adherent_attributes]
  end

  def set_medico_social_orgs
    @medico_social_orgs = Member.of_member_groups(settings.platform_medicosocial_orgs).by_name
  end

  def set_available_performances
    collaborating_clubs = Member.of_member_groups(settings.platform_collaborating_clubs)
    @available_performances = Performance.where(member_id: collaborating_clubs.ids).enabled
  end

  def notify_clubs_concerned_by_removal(members, removed_performance_ids)
    @failed_recipients ||= []
    members.each do |member|
      performances = member.performances.where(id: removed_performance_ids)
      begin
        SubscriptionsMailer.removed_performances(@subscription, performances).deliver_now
      rescue Exception => e
        @failed_recipients << member.name
      end
    end
  end

  def notify_clubs_concerned_by_addition(members, added_performance_ids)
    @failed_recipients ||= []
    members.each do |member|
      performances = member.performances.where(id: added_performance_ids)
      begin
        SubscriptionsMailer.added_performances(@subscription, performances).deliver_now
      rescue Exception => e
        @failed_recipients << member.name
      end
    end
  end
end
