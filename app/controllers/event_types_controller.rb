#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class EventTypesController < ApplicationController
  before_action :authorize, :except => [ :show ]

  def index
    @event_types = EventType.all
  end

  def show
    @event_type = EventType.find(params[:id])
    @seasoned_events = @event_type.seasoned_events
  end

  def new
    @event_type = EventType.new
  end

  def edit
    @event_type = EventType.find(params[:id])
  end

  def create
    @event_type = EventType.new(event_type_params)
    if @event_type.save
      flash[:notice] = "Type d'événement créé avec succès"
      redirect_to @event_type
    else
      render :action => "new"
    end
  end

  def update
    @event_type = EventType.find(params[:id])
    if @event_type.update_attributes(event_type_params)
      flash[:notice] = "Type d'événement mis à jour avec succès"
      redirect_to @event_type
    else
      render :action => "edit"
    end
  end

  def update_ga_events
    @event_type = EventType.find(params[:id])
    @event_type.events.not_in_ga.each do |event|
      event.save_ga_event
    end
    flash[:notice] = "La demande de création des événements GA a été lancée."
    redirect_to @event_type
  end

  def destroy
    @event_type = EventType.find(params[:id])
    if @event_type.destroy
      flash[:notice] = "Type d'événement supprimé avec succès"
    end
    redirect_to event_types_url
  end

  private

  def event_type_params
    params.require(:event_type).permit(:title, :description, :color, :should_have_gcal)
  end
end
