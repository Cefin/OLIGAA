#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class EventsController < ApplicationController
  before_action :authorize, :except => [ :show ]

  def index
    @passed_events = Event.reverse_chronology.visible.passed
    @seasoned_passed_events = Season.all_for(@passed_events)
    @current_events = Event.reverse_chronology.visible.current
    @events_to_come = Event.chronology.visible.to_come
    @unscheduled_events = Event.visible.unscheduled
  end

  def show
    @event = Event.find(params[:id])
    marker = {
      latlng: [@event.latitude, @event.longitude],
      title: @event.title,
      icon: :blue
    }
    @markers = [marker]
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      check_event_type
      flash[:notice] = "Evénément créé avec succès."
      redirect_to(@event)
    else
      flash[:error] = t('flash.error')
      render :action => "new"
    end
  end

  def update
    params[:event][:activity_ids] ||= []
    @event = Event.find(params[:id])
    if @event.update_attributes(event_params)
      check_event_type
      flash[:notice] = "Evénément modifié avec succès."
      redirect_to(@event)
    else
      flash[:error] = t('flash.error')
      render :action => "edit"
    end
  end

  def destroy
    @event = Event.find(params[:id])
    if @event.flag_as_destroyable
      flash[:notice] = "Evénément supprimé avec succès."
    end
    redirect_to(events_url)
  end

  private

  def check_event_type
    flash[:warning] = "L'événement n'a pas de type associé, il ne sera donc pas affiché dans le calendrier." if @event.event_type.nil?
  end

  def event_params
    params.require(:event).permit(:title, :description, :begin_at, :end_at, :location, :price, :event_type_id, activity_ids: [], attached_files_attributes: attached_file_permitted_attributes)
  end
end
