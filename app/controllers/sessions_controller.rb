#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class SessionsController < ApplicationController
  def new
  end

  def create
    @member = Member.find_by_login(params[:login])
    if !@member.nil? && @member.authenticate(params[:password])
      if @member.blocked?
        flash[:error] = "Désolé, vous ne pouvez plus vous connecter au site parce que vous avez été verrouillé. Merci de contacter les administrateurs du site pour régler le problème."
      else
        reset_session
        session[:member_id] = @member.id
        flash[:notice] = "Vous êtes maintenant connecté à la zone membre."
        if @member.first_connection?
          flash[:warning] = "Vous devez aller sur votre profil pour modifier votre mot de passe!"
          session[:must_change_password] = true
        end
      end
    else
      flash[:error] = "Désolé, vous ne pouvez accéder à cette zone."
    end
    redirect_to(root_path)
  end

  def destroy
    reset_session
    flash[:notice] = "Vous êtes maintenant déconnecté de la zone membre."
    redirect_to(root_path)
  end

end