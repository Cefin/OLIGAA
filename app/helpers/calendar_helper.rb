#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
include URLEncoding

module CalendarHelper
  def month_link(month_date)
    link_to I18n.localize(month_date, format: "%B"), calendar_url(month: month_date.month, year: month_date.year)
  end

  def year_options
    years = ""
    this_year = Date.today.year
    (-1..3).each do |i|
      new_year = this_year + i
      years += "<option" + (new_year.to_s == session[:cal_year] ? ' selected="selected"' : '') +">#{new_year}</option>"
    end
    years.html_safe
  end

  def date_from_session
    Date.new(session[:cal_year].to_i,session[:cal_month].to_i,1)
  end

  def labelize(text)
    my_url_encode(text).downcase
  end

end
