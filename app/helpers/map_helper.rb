#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module MapHelper

  def tailored_map(markers)
    update_icon_settings(markers)
    final_map = case markers.length
    when 0
      ""
    when 1
      map(markers: markers, center: {latlng: markers.first[:latlng], zoom: 13})
    else
      lower_marker, upper_marker = markers.minmax_by { |m| m[:latlng][0] }
      western_marker, eastern_marker = markers.minmax_by { |m| m[:latlng][1] }
      bottom_left_limit = [lower_marker[:latlng][0], western_marker[:latlng][1]]
      top_right_limit = [upper_marker[:latlng][0], eastern_marker[:latlng][1]]
      fitbounds = [bottom_left_limit, top_right_limit]
      map(markers: markers, fitbounds: fitbounds)
    end
    content_tag :div, id: 'map_container' do
      final_map
    end
  end

  def ban_data_gouv_fr_map
    textilize_without_paragraph 'Pour vous aider à préciser l\'adresse : "(externe)carte interactive":https://adresse.data.gouv.fr/map'
  end

  private

  def update_icon_settings(markers)
    markers.each do |marker|
      marker[:icon] = icon_settings(marker[:icon])
    end 
  end

  def icon_settings(color)
    {
      icon_url: ActionController::Base.helpers.asset_path("marker-icon-#{color}.png"),
      icon_size: [25, 41],
      icon_anchor: [12, 41],
      popup_anchor: [1, -34],
      shadow_size: [41, 41]
    }
  end
end
