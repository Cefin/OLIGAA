#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module SubscriptionsHelper
  def internal_subscription_label(subscription)
    "<span class=#{subscription.billing_status} title='#{t(subscription.billing_status)}'>€</span>
     <span class=\"platform-number\">#{subscription.platform_number}</span>
     <span>#{subscription.adherent.name}</span>
     <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>
     <span class=\"activities\">#{subscription.performances_list}</span>".html_safe
  end

  def external_subscription_label(subscription, member)
    "#{subscription.adherent.name} (#{subscription.member.name}) → #{subscription.performances.for_member(member).to_a.map(&:full_title).join(', ')}"
  end

  def card_header(subscription)
    label = (subscription.multipass? ? "Pass' Multi-Activités" : "Mono-Activité")
    header = ""
    4.times do
      header << content_tag(:span, label)
    end
    header.html_safe
  end
end
