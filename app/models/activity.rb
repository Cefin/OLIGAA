#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Activity < ApplicationRecord
  include SeasonedEventsConcern

  has_and_belongs_to_many :activity_categories, :join_table => :activities_cats
  has_and_belongs_to_many :events
  has_and_belongs_to_many :members
  has_and_belongs_to_many :novels
  has_many :attached_files, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attached_files, :allow_destroy => true
  has_attached_file :poster, :styles => { :thumb => "20x20", :standard => "120x120" }, :convert_options => { :all => '-strip -colorspace RGB'}
  validates_attachment_size :poster, :less_than => 1.megabyte, :message => "doit faire moins de 1 MB", :if => Proc.new { |file| !file.poster_file_name.blank? }
  do_not_validate_attachment_file_type :poster
  has_many :performances, dependent: :destroy

  default_scope { order('title ASC') }
  scope :filterable, -> { where(filterable: true) }
  scope :listed, -> { where(listed: true) }

  validates_presence_of :title
  validates_presence_of :presentation

  def self.uncategorized
    all.reject { |act| !act.activity_categories.blank? }
  end
end
