#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Event < ApplicationRecord
  belongs_to :event_type, optional: true
  has_many :attached_files, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attached_files, :allow_destroy => true
  has_and_belongs_to_many :activities

  geocoded_by :location
  after_validation :geocode, if: ->(obj){ obj.location.present? and obj.location_changed? }

  validates_presence_of :title, :description
  validate :end_date_after_begin_date

  scope :reverse_chronology, -> { order('begin_at DESC') }
  scope :chronology, -> { order('begin_at ASC') }
  scope :visible, -> { where(goto_trash: false) }
  scope :not_in_ga, -> { where(ga_evt_id: nil) }
  scope :with_dates, -> { where("begin_at is not null AND end_at is not null") }
  scope :passed, -> { where("end_at < ?", Time.now) }
  scope :to_come, -> { where("begin_at > ?", Time.now) }
  scope :current, -> { where("end_at > ? AND begin_at < ?", Time.now, Time.now) }
  scope :unscheduled, -> { where("end_at IS NULL OR begin_at IS NULL") }
  scope :between, ->(start_date, end_date) { where("begin_at BETWEEN ? AND ? OR end_at BETWEEN ? AND ?", start_date,end_date,start_date,end_date) }

  def self.for_list(month, year)
    selected_month = DateTime.new(year, month)
    start_date = selected_month.beginning_of_month
    end_date = selected_month.end_of_month
    between(start_date, end_date)
  end

  def self.for_calendar(month, year)
    selected_month = DateTime.new(year, month)
    start_date = selected_month.beginning_of_month - 8.days
    end_date = selected_month.end_of_month + 8.days
    between(start_date, end_date)
  end

  def color
    self.event_type.color
  end

  def to_come?
    self.begin_at && self.begin_at.to_date > Date.today
  end

  # Object methods

  def remaining_days
    self.begin_at.to_date - Date.today if self.to_come?
  end

  # GCal stuff

  after_save :save_ga_event

  def reset_ga_event
    self.update_column(:ga_evt_id, nil)
    self.update_column(:ga_cal_id, nil)
  end

  def save_ga_event
    if to_save = ga_event
      event_id, calendar_id = to_save.update(self)
      self.update_column(:ga_evt_id, event_id)
      self.update_column(:ga_cal_id, calendar_id)
    end
  end
  handle_asynchronously :save_ga_event

  def flag_as_destroyable
    self.update_column(:goto_trash, true)
    self.destroy_ga_event
    return true
  end

  handle_asynchronously :destroy

  def destroy_ga_event
    ga_event.destroy
  end
  handle_asynchronously :destroy_ga_event

  def ga_event
    GoogleAgenda::Event.find_by_id_or_create(self.ga_evt_id, self.ga_cal_id) unless self.event_type.nil?
  end

  protected

  def end_date_after_begin_date
    if end_at < begin_at
      errors.add(:end_at, " : la date de fin ne peut être antérieure à celle de début de l'événement ;)")
    end unless begin_at.blank? || end_at.blank?
  end

end
