#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Contact < ApplicationRecord
  has_many :phone_numbers, :dependent => :destroy
  accepts_nested_attributes_for :phone_numbers, :allow_destroy => true
  belongs_to :member
  belongs_to :contact_type, optional: true

  attr_accessor :new_function

  before_save :create_contact_type_from_function

  def create_contact_type_from_function
    unless new_function.blank?
      if ct = ContactType.find_by_title(new_function)
        self.contact_type = ct
      else
        create_contact_type(:title => new_function)
      end
    end
  end

  validates_presence_of :last_name, :first_name, :email
  validates_format_of :email, :with => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i

  def full_info
    info = "#{first_name} #{last_name}"
    info << (contact_type ? ", #{contact_type.title}" : "")
    info << " (#{email})"
  end

  def self.untyped
    where(contact_type_id: nil)
  end
end
