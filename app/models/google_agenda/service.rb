#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'google/api_client'
require 'google/api_client/auth/file_storage'
require 'google/api_client/client_secrets'

module GoogleAgenda
  class Service
    include Singleton
    KEY_FILE = File.join(Rails.root, "config", "google-oauth2", "keyfile.p12")

    def initialize
    end

    def api_client
      return @api_client unless @api_client.nil?

      @api_client = Google::APIClient.new(
      :application_name => "#{ENV['APP_NAME']} - #{Rails.env}",
      :application_version => '1.0.0')

      ## Load our credentials for the service account
      key = Google::APIClient::KeyUtils.load_from_pkcs12(KEY_FILE, "notasecret")

      @api_client.authorization = Signet::OAuth2::Client.new(
      :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
      :audience => 'https://accounts.google.com/o/oauth2/token',
      :scope => 'https://www.googleapis.com/auth/calendar',
      :issuer => ENV['GOOGLE_CLIENT_ID'],
      :signing_key => key,
      :person => Website.first.gcal_user)

      ## Request a token for our service account
      @api_client.authorization.fetch_access_token!

      @api_client
    end

    def calendar_api
      return @calendar_api unless @calendar_api.nil?

      @calendar_api = api_client.discovered_api('calendar', 'v3')
    end

    def self.reset!
      Singleton.send :__init__, self
    end
  end
end
