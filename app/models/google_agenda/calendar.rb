#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module GoogleAgenda
  class Calendar
    attr_reader :cal
    # Retrive GA Calendar by ID
    def self.find_by_id_or_create(id)
      if api_client = GoogleAgenda::Service.instance.api_client
        calendar_api = GoogleAgenda::Service.instance.calendar_api
        cal = id ? find_calendar_by_id(id, api_client, calendar_api) : create_calendar(api_client, calendar_api)

        self.new(cal)
      else
        raise "No GA service available to find/create event type #{id}."
      end
    end

    def initialize(cal=nil)
      @cal = cal
      @api_client = GoogleAgenda::Service.instance.api_client
      @calendar_api = GoogleAgenda::Service.instance.calendar_api
    end

    def update(obj)
      @cal['summary'] = obj.title
      @cal['description'] = obj.description

      result = @api_client.execute(api_method: @calendar_api.calendars.patch,
        parameters: {'calendarId' => @cal['id']},
        body: {'summary' => @cal['summary'], 'description' => @cal['description']}.to_json,
        headers: {'Content-Type' => 'application/json'})

      return @cal['id']
    end

    private

    def self.create_calendar api_client, calendar_api
      result = api_client.execute(api_method: calendar_api.calendars.insert, body: {
          'summary' => "TEMP #{DateTime.now}"
        }.to_json,
        :headers => {'Content-Type' => 'application/json'})

      JSON.parse(result.body)
    end

    def self.find_calendar_by_id id, api_client, calendar_api
      result = api_client.execute(api_method: calendar_api.calendars.get, parameters: {
        'calendarId' => id
        })

      JSON.parse(result.body)
    end
  end
end