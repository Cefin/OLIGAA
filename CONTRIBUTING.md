# Comment contribuer

OLIGAA est un logiciel libre et recueille avec intérêt tout avis ou contribution, n'hésitez donc pas à nous contacter.

Ce guide de contribution ne déroge pas à la règle :-)

## Donner votre avis

1. Vous pouvez nous écrire à **contact (at) cdsa33.org** ;
2. Formuler une demande (_issue_) dans [Framagit](https://git.framasoft.org/CDSA33/OLIGAA) (utilisant [Gitlab CE](https://about.gitlab.com/features/#community)).

Et discutons-en !

## Proposer du code

Il existe deux moyens de fournir du code pour OLIGAA, la première étant préférée :
1. Fourcher (_fork_) le projet sur [Framagit](https://git.framasoft.org/CDSA33/OLIGAA) et soumettre un [requête de fusion](https://git.framasoft.org/help/gitlab-basics/add-merge-request.md) (_merge request_) ;
2. Envoyer un correctif Git à **contact (at) cdsa33.org**.

Dans tous les cas, veuillez respecter la recommandation suivante.

### N'oubliez pas les tests

Votre correction aura bien plus de chances d'être intégré à l'outil si ce qu'elle améliore est bien testé. Si vous n'êtes pas à l'aise avec la batterie de tests actuelle, n'hésitez pas à nous en faire part.

À cette étape de lecture, vous devriez déjà savoir comment exécuter les tests, mais juste au cas où ;-)
```
bundle exec rake
```
